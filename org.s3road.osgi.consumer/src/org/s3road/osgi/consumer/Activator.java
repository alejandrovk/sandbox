package org.s3road.osgi.consumer;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.s3road.osgi.server.IServer;


public class Activator implements BundleActivator {

	private static BundleContext context;
	private IServer server;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;  
		// Register directly with the service
		ServiceReference<?> reference = context.getServiceReference(IServer.class.getName());
		server = (IServer) context.getService(reference);
		//start the server
		server.start();
//		if (server.startServer()){
//			ServerActivator.getLogger().log(LogService.LOG_INFO, "Started server!!!");
//		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
