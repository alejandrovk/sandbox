package org.s3road.reader;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.smool.kpi.common.Logger;

/**
 * Main entry point class
 * @author Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *
 */
public class Main {
	
	/**
	 * Used to validate IPv4 address
	 */
	private static final String PATTERN = 
	        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Set up the logging system
		Logger.setDebugging(true);
		Logger.setClassNameActivated(true);
		Logger.setDatetimeActivated(true);
		Logger.setDebugLevel(4);
		
		int length = args.length;
        if (length <= 0) {
            Logger.error("ERROR: No arguments provided");
            showHelp();
            System.exit(1);
        }
        if (length == 2){  
        	try {
        		int port = Integer.parseInt(args[0]);
                String address = args[1];
                //validate correct IP address
                if (validate(address)){
                    try {
                		Reader reader = new Reader(port, address);
                		reader.start();
                	} 
                	catch (IOException e) {
                		System.exit(1);
                	}
                }
                else{
                    Logger.error("Provided address must be in valid IPv4 format");
                    showHelp();
                    System.exit(1);
                }
            } 
            catch (NumberFormatException e) {
                Logger.error("PORT argurment must be an integer");
                showHelp();
                System.exit(1);
            }
        }
        else{
        	Logger.error("Incorrect number of arguments provided");
        	showHelp();
        	System.exit(1);
        }	
	}

	private static void showHelp() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("USAGE INSTRUCTIONS" + "\n");
		buffer.append("*******************************************" + "\n");
		buffer.append("You must provide a port and address where the application will be listening for broadcast messages." + "\n\n");
		buffer.append("For instance: java -jar reader.jar 12345 192.168.1.255" + "\n\n");
		buffer.append("Thus, first argument is the PORT and second is the ADDRESS." + "\n\n");
		buffer.append("PORT is a [d]+ numeric field and the ADDRESS is [d]+.[d]+.[d]+.[d]+ format" + "\n\n");
		buffer.append("You must provide both arguments for this app to work" + "\n\n");
		Logger.info(buffer.toString());
		
	}
	
	/**
	 * Checks if a provided String is an IPv4 address
	 * @param ip	The String
	 * @return	TRUE if String is IPv4, false otherwise
	 */
	public static boolean validate(final String ip){          

	      Pattern pattern = Pattern.compile(PATTERN);
	      Matcher matcher = pattern.matcher(ip);
	      return matcher.matches();             
	}

}
