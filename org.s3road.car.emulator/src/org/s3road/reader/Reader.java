/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *******************************************************************************/

package org.s3road.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.s3road.reader.connector.impl.BroadcastChannelReader;
import org.smool.kpi.common.Logger;

/**
 * Class that pushes random speed messages to the server
 * @author Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *
 */
public class Reader implements Runnable {

	
	/**
	 * The thread object
	 */
	private Thread thread;
	
	/**
	 * Lock to avoid expensive run()
	 */
	private Object lock;
	
	/**
	 * Control flag
	 */
	private boolean running;

	/**
	 * The reference to the Broadcast channel reader that reads incoming messages
	 */
	private BroadcastChannelReader channelReader;
	
	/**
	 * Constructor
	 * @param port		The listening port
	 * @param address	The broadcast address
	 * @throws IOException
	 */
    public Reader(int port, String address) throws IOException {
    	
		this.lock = new Object();
		//create the connector and start the threads
		try {
			channelReader = new BroadcastChannelReader("BroadcastChannelReader", port, address);
		}
		catch (IOException e) {
			Logger.error(this.getClass().getName(), "Could not create the broadcastChannelReader reference. Aborting. Reason is " + e.getMessage());
			throw new IOException(e);
		}
		
    }
    
    /**
     * Starts the thread
     */
    public void start(){
    	channelReader.start();
    	// start processing outoging messages
    	this.thread = new Thread(this, "Reader");
    	this.thread.start();
    }
	
	@Override
	public void run() {
		running = true;
		Logger.debug(this.getClass().getName(), "Reader Thread starting");
		
		while(running) {
			//ask for input and if the user enters something stop all threads and exit the app
			Logger.info("Press ENTER to kill the application...");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				br.readLine();
				//close the threads and terminate the app
				Logger.info("Gracefully terminating the app");
				channelReader.setRunning(false);
				channelReader.getSelector().wakeup();
				running = false;
			} 
			catch (IOException e) {
				Logger.error("An error ocurred trying to get user input. Finishing thread and closing app...");
				channelReader.setRunning(false);
				channelReader.getSelector().wakeup();
				running = false;
			}
			
		}
		Logger.debug(this.getClass().getName(), "Reader Thread stopped");
	}
	
	/**
	 * Used to gracefully stop the thread
	 * 
	 * @return	None
	 */
	public void destroy() {
			
		//set control flag
		running = false;
		thread = null;
		//we must notify locking threads that disconnection actually happened
		synchronized(lock) {
			lock.notifyAll();
		}
	}



}
