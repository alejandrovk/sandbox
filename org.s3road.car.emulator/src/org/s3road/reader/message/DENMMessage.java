/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)  
 *    
 *******************************************************************************/ 

package org.s3road.reader.message;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.smool.kpi.common.Logger;


/**
 * This abstract class implements the Message protocol
 * 
 * @author Alejandro Villamarin alejandro.villamarin@tecnalia.com
 */
public class DENMMessage extends AbstractMessage {

	/**
	 * Defines maximum DENM message length
	 */
	private static final int MESSAGE_LENGTH = 49;

	/**
	 * The protocol version used in the message - 1 byte
	 */
	private short protocolVersion;
	
	/**
	 * The message Id - 1 byte
	 */
	private short messageId;
	
	/**
	 * Station id that generates the message - 4 bytes
	 */
	private long stationId;
	
	/**
	 * Sequence number counter - 2 bytes
	 */
	private int seqNumber;
	
	/**
	 * Data version, counter that indicates if there has been some data modification - 1 byte
	 */
	private short dataVersion;
	
	/**
	 * Indicates the deadline for the message to be valid, beyond that, the message is invalid and should be deleted - 6 bytes
	 */
	private long  expirationDate;
	
	/**
	 * Frequency at which the messages are sent - 1 byte
	 */
	private short frequency;
	
	/**
	 * Indicates the probability that the event is in the indicated position - 7 bits???
	 */
	private short reliability;
	
	/**
	 * Indicates that an event sent in a previous DENM message from other station does not exist - 1 byte
	 */
	private boolean isNegated;
	
	/**
	 * Indicates the cause - 1 byte
	 */
	private short cause;
	
	/**
	 * Indicates the subcause - 1 byte
	 */
	private short subcause;
	
	/**
	 * Indicates the severity - 1 byte
	 */
	private short severity;
	
	/**
	 * Indicates the latitude - 4 bytes
	 */
	private long latitude;
	
	/**
	 * Indicates the longitude - 4 bytes
	 */
	private long longitude;
	
	/**
	 * Indicates the altitude - 2 bytes
	 */
	private int altitude;
	
	/**
	 * Indicates the denmDistance, the distance from the source measured like circle radius. One unit equals one meter - 2 bytes
	 */
	private int denmDistance;
	
	/**
	 * Indicates the traceLocId, an array of points that drive to the event position. There can be several traces to reach the event - 3 bits
	 */
	private short traceLocId;
	
	/**
	 * Indicates the number of consecutive points conforming the trace that leads to the event position - 5 bits
	 */
	private short noWP;
	
	/**
	 * Indicates the latitude of a consecutive point that conforms a trace - 4 bytes
	 */
	private long latitudeWP;
	
	/**
	 * Indicates the longitude of a consecutive point that conforms a trace- 4 bytes
	 */
	private long longitudeWP;
	
	/**
	 * Indicates the altitude of a consecutive point that conforms a trace - 2 bytes
	 */
	private int altitudeWP;
	
	
	/**
	 * Public constructor
	 */
	public DENMMessage() {
	}
	
	/**
	 * Constructor with a parameter used to populate each field
	 * @param rawMessage	A byte array that contains a raw DENM message
	 * @throws IllegalArgumentException		Thrown if provided array lenght is greated than the maximun allowed
	 */
	public DENMMessage(byte [] rawMessage) throws IllegalArgumentException {
		
		if (rawMessage.length != DENMMessage.MESSAGE_LENGTH) {
			throw new IllegalArgumentException("Cannot create a DENM Message with the wrong length");
		}
		else {
			protocolVersion = byte2short(rawMessage[0]);
			messageId = byte2short(rawMessage[1]);
			creationTime = bytes2time(rawMessage[2], rawMessage[3], rawMessage[4], rawMessage[5], rawMessage[6], rawMessage[7]);
			stationId = bytes2long(rawMessage[8], rawMessage[9], rawMessage[10], rawMessage[11]);
			seqNumber = bytes2int(rawMessage[12], rawMessage[13]);
			dataVersion = byte2short(rawMessage[14]);
			expirationDate = bytes2time(rawMessage[15], rawMessage[16], rawMessage[17], rawMessage[18], rawMessage[19], rawMessage[20]);
			frequency = byte2short(rawMessage[21]);
			reliability = (short) (byte2short(rawMessage[22]) / 2);
			isNegated = (byte2short(rawMessage[22]) % 2) > 0;
			cause = byte2short(rawMessage[23]);
			subcause = byte2short(rawMessage[24]);
			severity = byte2short(rawMessage[25]);
			latitude = bytes2long(rawMessage[26], rawMessage[27], rawMessage[28], rawMessage[29]);
			longitude = bytes2long(rawMessage[30], rawMessage[31], rawMessage[32], rawMessage[33]);
			altitude = bytes2int(rawMessage[34], rawMessage[35]);
			denmDistance = bytes2int(rawMessage[36], rawMessage[37]);
			traceLocId = (short) (byte2short(rawMessage[38]) / 8);
			noWP = (short) (byte2short(rawMessage[38]) % 8);
			latitudeWP = bytes2long(rawMessage[39], rawMessage[40], rawMessage[41], rawMessage[42]);
			longitudeWP = bytes2long(rawMessage[43], rawMessage[44], rawMessage[45], rawMessage[46]);
			altitudeWP = bytes2int(rawMessage[47], rawMessage[48]);
		}
	}

	public short getProtocolversion() {
		return protocolVersion;
	}

	public void setProtocolversion(short protocolversion) {
		this.protocolVersion = protocolversion;
	}

	public short getMessageId() {
		return messageId;
	}

	public void setMessageId(short messageId) {
		this.messageId = messageId;
	}

	public long getStationId() {
		return stationId;
	}

	public int getSeqNumber() {
		return seqNumber;
	}

	public short getDataVersion() {
		return dataVersion;
	}

	public long getExpirationDate() {
		return expirationDate;
	}

	public short getFrequency() {
		return frequency;
	}

	public short getReliability() {
		return reliability;
	}

	public boolean isNegated() {
		return isNegated;
	}

	public short getCause() {
		return cause;
	}

	public short getSubcause() {
		return subcause;
	}

	public short getSeverity() {
		return severity;
	}

	public long getLatitude() {
		return latitude;
	}

	public long getLongitude() {
		return longitude;
	}

	public int getAltitude() {
		return altitude;
	}

	public int getDenmDistance() {
		return denmDistance;
	}

	public short getTraceLocId() {
		return traceLocId;
	}

	public short getNoWP() {
		return noWP;
	}

	public float getLatitudeWP() {
		return latitudeWP;
	}

	public float getLongitudeWP() {
		return longitudeWP;
	}

	public int getAltitudeWP() {
		return altitudeWP;
	}

	public void setStationId(long stationId) {
		this.stationId = stationId;
	}

	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}

	public void setDataVersion(short dataVersion) {
		this.dataVersion = dataVersion;
	}

	public void setExpirationDate(long expirationDate) {
		this.expirationDate = expirationDate;
	}

	public void setFrequency(short frequency) {
		this.frequency = frequency;
	}

	public void setReliability(short reliability) {
		this.reliability = reliability;
	}

	public void setNegated(boolean isNegated) {
		this.isNegated = isNegated;
	}

	public void setCause(short cause) {
		this.cause = cause;
	}

	public void setSubcause(short subcause) {
		this.subcause = subcause;
	}

	public void setSeverity(short severity) {
		this.severity = severity;
	}

	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}

	public void setAltitude(int altitude) {
		this.altitude = altitude;
	}

	public void setDenmDistance(int denmDistance) {
		this.denmDistance = denmDistance;
	}

	public void setTraceLocId(short traceLocId) {
		this.traceLocId = traceLocId;
	}

	public void setNoWP(short noWP) {
		this.noWP = noWP;
	}

	public void setLatitudeWP(long latitudeWP) {
		this.latitudeWP = latitudeWP;
	}

	public void setLongitudeWP(long longitudeWP) {
		this.longitudeWP = longitudeWP;
	}

	public void setAltitudeWP(int altitudeWP) {
		this.altitudeWP = altitudeWP;
	}


	/**
	 * Gets the byte[] representation of the DENMMessage
	 * @return a byte[] array
	 */
	@Override
	public byte[] toByteArray() {
		ByteBuffer buffer = ByteBuffer.allocate(49);
		
		/**********************************************************************************************************************************************
		 * Insertion order, from MSB to LSB should be:
		 * 
		 * -------------------------------------------------------------------------------------------------------------------------------------------
		 * |               |         |            |         |          |           |              |         |           |         |       |....
		 * |ProtocolVersion|messageId|creationTime|stationId|sequenceNº|dataVersion|expirationTime|frequency|reliability|isNegated|  cause|
		 * |     1 byte    | 1 byte  |  6 bytes   | 4 bytes | 2 bytes  |  1 byte   |  6 bytes     | 1 byte  |  7 bits   |  1 bit  |1 byte |
		 * |               |         |            |         |          |           |              |         |           |         |       |....
		 * -------------------------------------------------------------------------------------------------------------------------------------------
		 * 
		 * ---------------------------------------------------------------------------------------------------------------------
		 * ....|        |        |        |         |        |            |          |       |          |           |           |
		 * ....|subcause|severity|latitude|longitude|altitude|denmDistance|traceIdLoc|  noWP |latitudeWP|longitudeWP|altitudeWWP|
		 * ....| 1 byte | 1 byte | 4 bytes| 4 bytes | 2 bytes|  2 bytes   |  3 bits  |5 bits |  4 bytes |  4 bytes  |  2 bytes  |
		 * ....|        |        |        |         |        |            |          |       |          |           |           |
		 * ---------------------------------------------------------------------------------------------------------------------
		 *  
		 *********************************************************************************************************************************************/

		//add the protocol version to the bitSet
		buffer.put(short2Byte(protocolVersion));
		buffer.put(short2Byte(messageId));
		buffer.put(time2Bytes(creationTime));
		buffer.put(to4Bytes(stationId));
		buffer.put(to2Bytes(seqNumber));
		buffer.put(short2Byte(dataVersion));
		buffer.put(time2Bytes(expirationDate));
		buffer.put(short2Byte(frequency));
		byte reliabilityIsNegated = short2Byte((short) ((reliability % 128) * 2 + (isNegated ? 1 : 0)));
		buffer.put(reliabilityIsNegated);
		buffer.put(short2Byte(cause));
		buffer.put(short2Byte(subcause));
		buffer.put(short2Byte(severity));
		buffer.put(to4Bytes(latitude));
		buffer.put(to4Bytes(longitude));
		buffer.put(to2Bytes(altitude));
		buffer.put(to2Bytes(denmDistance));
		byte traceLocIdNoWP = short2Byte((short) ((traceLocId % 8)*32 + (noWP % 32)));
		buffer.put(traceLocIdNoWP);
		buffer.put(to4Bytes(latitudeWP));
		buffer.put(to4Bytes(longitudeWP));
		buffer.put(to2Bytes(altitudeWP));
		
		
		byte [] ret = buffer.array();

		Logger.debug("Number of bytes in message is: " + ret.length);	
		
		return ret;
		
	}
	
	/**
	 * Given an integer, returns the byte array representing it
	 * @param in	The integer to be converted
	 * @return	The byte array representation
	 */
	private byte [] to2Bytes(int in) {
		ByteBuffer ret = ByteBuffer.allocate(2);
		int val = in % 65536;
		
		short s1 = (short) (val / 256);
		short s0 = (short) (val % 256);
		
		ret.put(short2Byte(s1));
		ret.put(short2Byte(s0));
		
		return ret.array();
	}

	/**
	 * Given an long, returns the byte array representing it
	 * @param in	The long to be converted
	 * @return	The byte array representation
	 */
	private byte [] to4Bytes(long in) {
		ByteBuffer ret = ByteBuffer.allocate(4);
		long div = 4294967296L;
		long val = in % div;
		int rem = 0;
		
		short s3 = (short) (val / 16777216L);
		rem = (int) (val % 16777216L);
		short s2 = (short) (rem / 65536);
		rem = rem % 65536;
		short s1 = (short) (rem / 256);
		short s0 = (short) (rem % 256);
		
		ret.put(short2Byte(s3));
		ret.put(short2Byte(s2));
		ret.put(short2Byte(s1));
		ret.put(short2Byte(s0));
		
		return ret.array();
	}

	/**
	 * Given an epoch timestamp, returns the byte array representing it
	 * @param time	The epoch timestamp to be converted
	 * @return	The byte array representation
	 */
	private byte [] time2Bytes(Long time) {
		ByteBuffer ret = ByteBuffer.allocate(6);
		String hex = Long.toHexString(time).toUpperCase();
		while (hex.length() < 12) {
			hex = "0" + hex;
		}
		
		while (hex.length() > 12) {
			hex = hex.substring(1);
		}
		
		try {
			for (int i = 0; i < 6; i++) {
				String strByte = "" + hex.charAt(i*2) + hex.charAt(i*2 + 1);
				short b = Short.parseShort(strByte, 16);
				if (b > 127) {
					b -= 256;
				}
				ret.put((byte) b);
			}
		}
		catch (NumberFormatException e) {
			// Exception captured for correctness
			e.printStackTrace();
		}
		
		
		return ret.array();
	}
	
	/**
	 * Given 6 bytes, creates and returns an epoch timestamp
	 * @param b0	The first byte
	 * @param b1	The second byte
	 * @param b2	The third byte
	 * @param b3	The fourth byte
	 * @param b4	The fifth byte
	 * @param b5	The sixth byte
	 * @return	The epoch timestamp, as a long
	 */
	private long bytes2time(byte b5, byte b4, byte b3, byte b2, byte b1, byte b0) {
		long l5, l4, l3, l2, l1, l0;
		l5 = byte2short(b5) * 1099511627776L;
		l4 = byte2short(b4) * 4294967296L;
		l3 = byte2short(b3) * 16777216L;
		l2 = byte2short(b2) * 65536L;
		l1 = byte2short(b1) * 256L;
		l0 = byte2short(b0) * 1L;
		return  l5 + l4 + l3 + l2 + l1 + l0;
	}
	
	/**
	 * Given 4 bytes, creates and returns a long
	 * @param b0	The first byte
	 * @param b1	The second byte
	 * @param b2	The third byte
	 * @param b3	The fourth byte
	 * @return	The long to be returned
	 */
	private long bytes2long(byte b3, byte b2, byte b1, byte b0) {
		long l3, l2, l1, l0;
		l3 = byte2short(b3) * 16777216L;
		l2 = byte2short(b2) * 65536L;
		l1 = byte2short(b1) * 256L;
		l0 = byte2short(b0) * 1L;
		return  l3 + l2 + l1 + l0;
	}
	
	/**
	 * Given 2 bytes, creates and returns an integer
	 * @param b0	The first byte
	 * @param b1	The second byte
	 * @return	The int to be returned
	 */
	private int bytes2int(byte b1, byte b0) {
		return (int)byte2short(b1) * 256 + (int)byte2short(b0);
	}
	
	/**
	 * Given a byte, creates and returns a short
	 * @param b		The byte
	 * @return	The short 
	 */
	private short byte2short(byte b) {
		if (b < 0) {
			return (short) (b+256);
		}
		return (short)b;
	}

	/**
	 * Given a short, creates and returns a byte
	 * @param b		The short
	 * @return	The byte (8 LSB from provided short) 
	 */
	private byte short2Byte(short s) {
		if (s < 128) {
			return (byte) s;
		}
		else {
			return (byte) (s-256);
		}
	}

	/**
	 * String representation of the DENMessage
	 */
	@Override
	public String toString(){
		
		Date date = new Date(creationTime);  
        Calendar cal = new GregorianCalendar();  
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MMM/dd hh:mm:ss z");  
        sdf.setCalendar(cal);  
        cal.setTime(date);  
        String creationTime = sdf.format(date);
        date = new Date(expirationDate);
        cal.setTime(date);
        String expirationTime = sdf.format(date);
		
		StringBuilder b = new StringBuilder();
		b.append("Contents of DENMMessage:                          " + "\n");
		b.append("**************************************************" + "\n");
		b.append("Protocol version:" + protocolVersion + "\n");
		b.append("Message ID:" + messageId + "\n");
		b.append("Creation Time:" + creationTime + "\n");
		b.append("Station ID:" + stationId + "\n");
		b.append("Sequence Number:" + seqNumber + "\n");
		b.append("Data Version:" + dataVersion + "\n");
		b.append("Expiration Time:" + expirationTime + "\n");
		b.append("Frequency:" + frequency + "\n");
		b.append("Reliability:" + reliability + "\n");
		b.append("Negated:" + isNegated + "\n");
		b.append("Cause:" + cause + "\n");
		b.append("Subcause:" + subcause + "\n");
		b.append("Severity:" + severity + "\n");
		b.append("Latitude:" + latitude + "\n");
		b.append("Longitude:" + longitude + "\n");
		b.append("Altitude:" + altitude + "\n");
		b.append("Distance:" + denmDistance + "\n");
		b.append("Trace ID:" + traceLocId + "\n");
		b.append("WP number:" + noWP + "\n");
		b.append("WP Latitude:" + latitudeWP + "\n");
		b.append("WP Longitude:" + longitudeWP + "\n");
		b.append("WP Altitude:" + altitudeWP + "\n");
		
		return b.toString();
	}
}
