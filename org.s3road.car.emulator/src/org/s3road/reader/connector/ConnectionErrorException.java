/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *******************************************************************************/ 

package org.s3road.reader.connector;

public class ConnectionErrorException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ConnectionErrorException() {
		super();
	}
	
	public ConnectionErrorException(String msg) {
		super(msg);
	}	

	public ConnectionErrorException(String msg, Throwable cause) {
		super(msg, cause);
	}	
	
}
