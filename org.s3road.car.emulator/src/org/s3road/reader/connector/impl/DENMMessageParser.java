/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   
 *******************************************************************************/ 

package org.s3road.reader.connector.impl;

import java.nio.charset.Charset;
import java.util.ArrayList;

import org.s3road.reader.message.DENMMessage;



/**
 * DENMessage parse class, used to parse/unparse byte arrays into DENMessage POJOs
 * @author Alejandro Villlamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *
 */
public class DENMMessageParser extends ClientConnectionAttachment {
	
	//Since DENMessages are not text-based, this is useless, still it would be nice a way to delimit  a DENMMessage, but the standard
	//says nothing about some kind of field that says Beginning of Message, End of Message
	private final static String START_TOKEN = "<SSAP_MESSAGE>";
	private final static String END_TOKEN = "</SSAP_MESSAGE>";
	
	private ArrayList<Integer> indexes = new ArrayList<Integer>();
	
	public DENMMessageParser() {
	}
	
	public int[] parseMessages(byte[] buffer) {
		
		String s;
		try {
			if (Charset.isSupported("UTF-8")) {
				s = new String(buffer, Charset.forName("UTF-8")).toUpperCase();
			}
			else {
				s = new String(buffer).toUpperCase();
			}
		} catch(Exception e) {
			return null;
		}
		
		indexes.clear();
		int fromIndex = 0;
		int pos = 0;
		while(pos != -1) {
			pos = s.indexOf(DENMMessageParser.START_TOKEN, fromIndex);
			if(pos != -1) {			
				// Start token found.
				fromIndex = pos;
				pos = s.indexOf(DENMMessageParser.END_TOKEN, fromIndex + DENMMessageParser.START_TOKEN.length());
				if(pos != -1) {
					indexes.add(fromIndex);
					indexes.add(pos + DENMMessageParser.END_TOKEN.length()-1);
					fromIndex = pos + DENMMessageParser.END_TOKEN.length();
				}
			}
		}
		
		if(indexes.size() == 0) {
			return null;
		} else {
			int[] idx = new int[indexes.size()];
			for(int i = 0; i < indexes.size(); i++) {
				idx[i] = indexes.get(i);
			}
			return idx;
		}
	}
	
	/**
	 * Given a byte array, tries to build a DENMMessage and returns it
	 * @param buffer The array containing the message for the DENMMessage
	 * @return	The DENMMessage if successful, null otherwise
	 */
	public DENMMessage parseDENMMessages(byte[] buffer) {
		
		/**********************************************************************************************************************************************
		 * Extraction order, from MSB to LSB should be:
		 * 
		 * -------------------------------------------------------------------------------------------------------------------------------------------
		 * |               |         |            |         |          |           |              |         |           |         |       |....
		 * |ProtocolVersion|messageId|creationTime|stationId|sequenceNº|dataVersion|expirationTime|frequency|reliability|isNegated|  cause|
		 * |     1 byte    | 1 byte  |  6 bytes   | 4 bytes | 2 bytes  |  1 byte   |  6 bytes     | 1 byte  |  7 bits   |  1 bit  |1 byte |
		 * |               |         |            |         |          |           |              |         |           |         |       |....
		 * -------------------------------------------------------------------------------------------------------------------------------------------
		 * 
		 * ---------------------------------------------------------------------------------------------------------------------
		 * ....|        |        |        |         |        |            |          |       |          |           |           |
		 * ....|subcause|severity|latitude|longitude|altitude|denmDistance|traceIdLoc|  noWP |latitudeWP|longitudeWP|altitudeWWP|
		 * ....| 1 byte | 1 byte | 4 bytes| 4 bytes | 2 bytes|  2 bytes   |  3 bits  |5 bits |  4 bytes |  4 bytes  |  2 bytes  |
		 * ....|        |        |        |         |        |            |          |       |          |           |           |
		 * ---------------------------------------------------------------------------------------------------------------------
		 *  
		 *********************************************************************************************************************************************/
		
		if (buffer != null){
		
//			ByteBuffer byteBuffer = ByteBuffer.allocate(buffer.length);
//			byteBuffer.put(buffer);
//			byteBuffer.rewind();
//			DENMMessage ret = new DENMMessage();
//			
//			
//			byte protocolVersion = byteBuffer.get();
//			ret.setProtocolversion(protocolVersion);
//			byte messageID = byteBuffer.get();
//			ret.setMessageId(messageID);
//			long creationTime = byteBuffer.getLong();
//			ret.setCreationTime(creationTime);
//			int stationId = byteBuffer.getInt();
//			ret.setStationId(stationId);
//			short sequenceNumber = byteBuffer.getShort();
//			ret.setSeqNumber(sequenceNumber);
//			byte dataVersion = byteBuffer.get();
//			ret.setDataVersion(dataVersion);
//			long expirationTime = byteBuffer.getLong();
//			ret.setExpirationDate(expirationTime);
//			byte freq = byteBuffer.get();
//			ret.setFrequency(freq);
//			byte reliability = byteBuffer.get();
//			ret.setReliability(reliability);
//			byte isNegated = byteBuffer.get();
//			ret.setNegated(isNegated);
//			byte cause = byteBuffer.get();
//			ret.setCause(cause);
//			byte subcause = byteBuffer.get();
//			ret.setSubcause(subcause);
//			byte severity = byteBuffer.get();
//			ret.setSeverity(severity);
//			float latitude = byteBuffer.getFloat();
//			ret.setLatitude(latitude);
//			float longitude = byteBuffer.getFloat();
//			ret.setLongitude(longitude);
//			short altitude = byteBuffer.getShort();
//			ret.setAltitude(altitude);
//			short denmDistance = byteBuffer.getShort();
//			ret.setDenmDistance(denmDistance);
//			byte traceId = byteBuffer.get();
//			ret.setTraceLocId(traceId);
//			byte wp = byteBuffer.get();
//			ret.setNoWP(wp);
//			float latitudeWP = byteBuffer.getFloat();
//			ret.setLatitudeWP(latitudeWP);
//			float longitudeWP = byteBuffer.getFloat();
//			ret.setLongitudeWP(longitudeWP);
//			short altitudeWP = byteBuffer.getShort();
//			ret.setAltitudeWP(altitudeWP);
			
			
			return new DENMMessage(buffer);
			
		}
		
		return null;
	
	}

}
