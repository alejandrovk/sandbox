package org.s3road.reader.connector.impl;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

import org.s3road.reader.message.DENMMessage;
import org.smool.kpi.common.Logger;


public class BroadcastChannelReader extends Thread{
	
	/**
	 * Debugging TAG
	 */
	private final String DEBUG_TAG = "MultiCastChannelReader";

	/**
	 * The queue containing the messages
	 */
	private ArrayBlockingQueue<DENMMessage> incomingQueue;
	
	/**
	 * A thread-blocking lock
	 */
	private Object lock;
	
	/**
	 * The datagram channel used to read messages
	 */
	private DatagramChannel channel;
	
	/**
	 * Thread control flag
	 */
    private boolean running = true;
    
    /**
     * The broadcast port to be used
     */
    private int broadcast_port = 23005;
    
    /**
     * The broadcast address to be used
     */
    private String broadcast_address = "192.168.1.255";
    
    /**
     * The address required to connect to the broadcast group
     */
    private InetSocketAddress address;
    
    /**
     * The queue maximum size
     */
    private final int WRITE_QUEUE_SIZE = 1024;

    /**
     * The selected to be used in the channel
     */
	private Selector selector;

	/**
	 * The buffer that stores incoming bytes
	 */
	private ByteBuffer readBuffer = ByteBuffer.allocate(32768);

    /**
     * Constructor
     * @param	port		The broadcast port
     * @param	address		The broadcast address to use
     * @throws IOException
     */
    public BroadcastChannelReader(int port, String address) throws IOException {
        this("MultiCastChannelReader", port, address);
    }
    
    /**
     * Constructor
     * @param name	The class name
     * @param	port		The broadcast port
     * @param	address		The broadcast address to use
     * @throws IOException
     */
    public BroadcastChannelReader(String name, int port, String address) throws IOException {
    	super(name);
    	broadcast_port = port;
    	broadcast_address = address;
    	this.address = new InetSocketAddress(broadcast_address, broadcast_port);
        this.lock = new Object();
        incomingQueue = new ArrayBlockingQueue<DENMMessage>(WRITE_QUEUE_SIZE);
    }

    
    /**
     * Run method. Waits for the queue to be populated and the sends the messages 
     */
    public void run() {
    	Logger.info(DEBUG_TAG, "Starting the BroacastChannelReader thread...");
    	try {
    		ByteBuffer.allocate(32768);
        	//Open the channel
        	channel = DatagramChannel.open();
        	channel.configureBlocking(false);
        	Logger.info(DEBUG_TAG, "Binding to address " + address.toString());
        	channel.socket().bind(address);
        	InetSocketAddress remoteAddress = new InetSocketAddress(broadcast_port);
        	channel.socket().connect(remoteAddress);
        	selector = Selector.open();
        	channel.register(selector, SelectionKey.OP_READ, new DENMMessageParser());
    	}
    	catch (IOException e) {
        	Logger.error(DEBUG_TAG, "An error occured trying to read a Datagram message. Reason is " + e.getMessage());
            running = false;
        }
    	
    	while (running) {
            select();
        }	
    }
    
	private void select() {
		try {
			// This is a blocking select that will return when new data arrive.
			selector.select();
			Set<SelectionKey> readyKeys = selector.selectedKeys();

			Iterator<SelectionKey> i = readyKeys.iterator();
			while (i.hasNext()) {
				SelectionKey key = i.next();
				i.remove();
				DatagramChannel channel = (DatagramChannel) key.channel();
				Object client = key.attachment();
				readBuffer.clear();
				try {
					// Read from the channel.
					long readBytes = channel.read(readBuffer);
					
					// We detect end-of-stream when channel.read(...) return -1.
					if(readBytes == -1) {
						// We raise a connection closed event only when both input and output streams
						// are closed.
						if( channel.socket().isClosed()) {
							Logger.info(DEBUG_TAG, "Connection [" + client.toString() + "] closed.");
//							client.fireConnectionClosed();
//							connectionlistener.connectionDestroyed(client);
						}
					} else {
						// Add read bytes to the attachment.
						((ClientConnectionAttachment)client).add(readBuffer);
						((ClientConnectionAttachment)client).checkForMessages();
//							Iterator<ClientMessage> msgList = ((ClientConnectionAttachment)client).getMessages();
//							while (msgList.hasNext()){
//								ClientMessage msg = msgList.next();
//								Logger.debug("Got a message : " + msg.toString()); 
//										
//							}
//							
//						}
					}
				} 
				catch(IOException ioe) {
					// The client has closed abruptly.
					Logger.error(DEBUG_TAG, "Connection [" + client.toString() + "] closed. Reason is " + ioe.getMessage());
					running = false;
//					client.fireConnectionClosed();
//					connectionlistener.connectionDestroyed(client);
				}
			}
		}
		catch (Exception e) {
			Logger.error(DEBUG_TAG, "Exception during select(): " + e.toString());
			running = false;
		}
		
	}

	/**
	 * Shutdown the thread if the queue is empty only
	 */
	public boolean shutdown() {
		if (incomingQueue.isEmpty()){
			Logger.info(DEBUG_TAG, "Shutting down MultiCastWriter...");
			running = false;
			this.interrupt();
			Logger.info(DEBUG_TAG, "MultiCastWriter stopped");
			return true;
		}
		else{
			Logger.error(DEBUG_TAG, "Could not shutdown the MulticastChannelReader");
			return false;
		}
	}
	
	/**
	 * Adds a message to the messages queue. 
	 * 
	 * @param 	message 	The DENMMessage object that contains the data
	 * @return	None
	 */
	public void addIncomingMessage(DENMMessage message) {
		incomingQueue.add(message);
		//Notify the waiting threads
		synchronized(lock) {
			lock.notifyAll();
		}
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public Selector getSelector() {
		return selector;
	}

	public void setSelector(Selector selector) {
		this.selector = selector;
	}


}
