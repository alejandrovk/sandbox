/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *******************************************************************************/ 

package org.s3road.reader.connector;


/**
 * The ModelException is related with exceptions arised
 * in model
 */
@SuppressWarnings("serial")
public class ClientModelException extends Exception {
	
	public ClientModelException() {
		
	}
	
	public ClientModelException(String message) {
		super(message);
	}

	public ClientModelException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
