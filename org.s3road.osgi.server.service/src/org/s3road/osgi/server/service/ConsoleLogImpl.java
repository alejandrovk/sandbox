/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Author:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *    
 *******************************************************************************/ 

package org.s3road.osgi.server.service;

import java.text.DateFormat;
import java.util.Date;

import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;

/**
 * Class used to output logging messages
 * @author Alejandro Villamarin - Tecnlia alejandro.villamarin@tecnalia.com
 *
 */
public class ConsoleLogImpl implements LogListener
{
    public void logged(LogEntry log)
    {
    	// Make a new Date object. It will be initialized to the current time.
        Date now = new Date();
        String timestamp = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(now);
        String logLevel = new String(); 
        int level = log.getLevel();
        switch (level){
        case 1:
        	logLevel = "ERROR";
        	break;
        case 2:
        	logLevel = "WARNING";
        	break;
        case 3:
        	logLevel = "INFO";
        	break;
        case 4:
        	logLevel = "DEBUG";
        	break;
        default:
        	logLevel = "NONE";
        }
       
        if (log.getMessage() != null)
            System.out.println("[" + logLevel + "]" + " [" + log.getBundle().getSymbolicName() + "] " +  "[" + timestamp + "] " + log.getMessage());

    }
}
