/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Authors:
 *    Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *******************************************************************************/ 

package org.s3road.osgi.server.service;

public class MessageParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MessageParseException() {
		super();
	}
	
	public MessageParseException(String message) {
		super(message);
	}

}