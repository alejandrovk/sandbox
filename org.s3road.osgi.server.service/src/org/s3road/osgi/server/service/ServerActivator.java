/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Author:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *    
 *******************************************************************************/ 

package org.s3road.osgi.server.service;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.LinkedList;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.s3road.osgi.server.IServer;
import org.s3road.osgi.server.IServerFactory;
import org.s3road.osgi.server.service.tcpip.core.TCPIPServerConfiguration;
import org.s3road.osgi.server.service.tcpip.core.TCPIPServerFactory;



/**
 * Activator class for the implementation of the IServer service
 * @author Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *
 */
public class ServerActivator implements BundleActivator {

	/** The plug-in id */
	public static final String PLUGIN_ID = "org.s3road.osgi.server.service";

	/** The shared instance */
	@SuppressWarnings("unused")
	private static ServerActivator plugin;
	
	/** The bundle context */
	private static BundleContext bc;
	
	/** The manager responsible for creating/destroying TCP/IP server instances */
	private TCPIPServerFactory manager;
	
	/**
	 * List with all the available LogReaderServices 
	 */
	private LinkedList<LogReaderService> m_readers = new LinkedList<LogReaderService>();
	
	/**
	 * List with all the available LogServices
	 */
	private LinkedList<LogService> m_services = new LinkedList<LogService>();
	
	/**
	 * Reference to the LogService that will eventually be used for logging
	 */
	private static LogService logger;
	
	/**
	 * Reference to our implementation of LogListener class, that prints Log messages in our desired format
	 */
	private ConsoleLogImpl m_console = new ConsoleLogImpl();
	
	/* 
	 *  We use a ServiceListener to dynamically keep track of all the LogReaderService service being
     * 	registered or unregistered
     * 
     */
    private ServiceListener m_servlistener = new ServiceListener() {
        public void serviceChanged(ServiceEvent event)
        {
            BundleContext bc = event.getServiceReference().getBundle().getBundleContext();
            LogReaderService lrs = (LogReaderService)bc.getService(event.getServiceReference());
            if (lrs != null)
            {
                if (event.getType() == ServiceEvent.REGISTERED)
                {
                    m_readers.add(lrs);
                    lrs.addLogListener(m_console);
                } else if (event.getType() == ServiceEvent.UNREGISTERING)
                {
                    lrs.removeLogListener(m_console);
                    m_readers.remove(lrs);
                }
            }
        }
    };

	static BundleContext getContext() {
		return bc;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void start(BundleContext bundleContext) throws Exception {
		
		plugin = this;
		
		ServerActivator.bc = bundleContext;
		
		// Get a list of all the registered LogReaderService, and add the console listener
    	ServiceTracker logReaderTracker = new ServiceTracker(bundleContext, org.osgi.service.log.LogReaderService.class.getName(), null);
        logReaderTracker.open();
        Object[] readers = logReaderTracker.getServices();
        if (readers != null) {
            for (int i=0; i<readers.length; i++)
            {
                LogReaderService lrs = (LogReaderService)readers[i];
                m_readers.add(lrs);
                //specify what LogListener will spit log messages
                lrs.addLogListener(m_console);
            }
        }
        //close the tracker 
        logReaderTracker.close();
       
        // Add the ServiceListener, but with a filter so that we only receive events related to LogReaderService
        String filter = "(objectclass=" + LogReaderService.class.getName() + ")";
        try {
            bundleContext.addServiceListener(m_servlistener, filter);
        } 
        catch (InvalidSyntaxException e) {
            System.out.println("ERROR: Could not add service Listeners for LogReaderService.");
        }
        
        // Get a list of all the registered LogService, and add the console listener
        ServiceTracker logServiceTracker = new ServiceTracker(bundleContext, org.osgi.service.log.LogService.class.getName(), null);
        logServiceTracker.open();
        Object[] services = logServiceTracker.getServices();
        if (services != null) {
            for (int i=0; i < services.length; i++) {
            	LogService ls = (LogService)services[i];
            	String className = ls.getClass().getSimpleName();
            	if (ls instanceof LogService && className.equals("LogServiceImpl")) {
            		m_services.add(ls);
            		logger = (LogService) ls;
            		logger.log(LogService.LOG_INFO, "Starting the " + PLUGIN_ID + " service..."); 
            		break;
            	}
                
            }
        }
        //we can close the service tracker now
        logServiceTracker.close();
        
        Dictionary props = new Hashtable();
		props.put("Server", "TCP/IP");

		manager = new TCPIPServerFactory(bundleContext);
		bundleContext.registerService(IServerFactory.class.getName(), manager, props);
		System.out.println("Manager for TCP/IP registered");
		ServiceTracker tracker = new ServiceTracker(bundleContext, IServer.class.getName(), manager);
		tracker.open();
        
		//Create a new TCPIPServer
		manager.create( "server001", TCPIPServerConfiguration.getDefaultProperties());
        
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
//		ServerActivator.bc = null;
		// stop the server.
		manager.shutdown();
		manager = null;
		System.out.println("Manager for TCP/IP unregistered");
	}
	
	/**
	 * Gets the current logger
	 * @return the logger
	 */
	public static LogService getLogger() {
		return logger;
	}

	/**
	 * Sets the logger
	 * @param logger the Logge
	 */
	public static void setLogger(LogService logger) {
		ServerActivator.logger = logger;
	}

}
