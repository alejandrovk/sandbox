/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *******************************************************************************/ 

package org.s3road.osgi.server.service.tcpip.core;

import org.s3road.osgi.server.AbstractMessage;
import org.s3road.osgi.server.IServer;
import org.s3road.osgi.server.IServerCommunication;


public class Session implements IServerCommunication, ServerMessageListener {

	/** ID generator */
	private static long nextId;

	/** The identifier of the connector session */
	private long id;

	/** The Connector */
	private Connector connector;
	
	/** Client name */
	private String clientName;
		
	/** The server */
	private IServer server;
	
	
	/** Constructor */
	public Session() {
		this.id = getNextId();
	}
	
	private synchronized long getNextId() {
		return ++nextId;
	}
	
	public void setServer (IServer server) {
		this.server = server;
	}
	
	/**
	 * Is called when the client closes its connection. If the connection is persistent, this method
	 * notifies to the Server that the connection has been closed. 
	 */
	@Override
	public void connectionClosed() {
//		if(connector.isPersistent()) {
//			try {
//				this.getServer().disconnect(clientName, CLOSED);
//			} 
//			catch(Exception e) {
//				e.printStackTrace();
//			}
//		}
	}

	/**
	 * Sends to Server a connection closed error. 
	 */
	@Override
	public void connectionError(String errorMsg) {
//		if(connector.isPersistent()) {
//			try {
//				this.getServer().disconnect(clientName, errorMsg);
//			} 
//			catch(Exception e) {
//				e.printStackTrace();
//			}
//		}
	}

	/**
	 * Is called when the client connection has a timeout. If the connection is persistent, 
	 * this method notifies to the Server that the connection has a timeout. 
	 */
	@Override
	public void connectionTimeout() {
//		if(connector.isPersistent()) {
//			try {
//				this.getSever().disconnect(clientName, TIMEOUT_ERROR);
//			} 
//			catch(Exception e) {
//				e.printStackTrace();
//			}
//		}
	}

	/**
	 * Sends a message to the Server.
	 * Gets the Server reference and sends the Message request. 
	 * @param The message to be sent
	 */
	@Override
	public void sendToServer(AbstractMessage request) {
//		try {
////			this.clientName = request;
////			this.getServer().connect(request);
//			
//			
//		} 
//		catch(ServerException e) {
//			
//			e.printStackTrace();
//		}

	}

	/**
	 * Sets the connector to this session
	 * @param connector the connector to set
	 */
	public void setConnector(Connector connector) {
		this.connector = connector;
		this.connector.setSIBSession(this);
	}

	/**
	 * @return the connector
	 */
	public Connector getConnector() {
		return connector;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	public IServer getServer() {
		return server;
	}

	@Override
	public void messageReceived(AbstractMessage message) {
		connector.messageReceivedFromSIB(message);
	}

	public void close() {
		if(connector != null) {
			connector.close();
		}
	}

	public String getClientName() {
		return clientName;
	}


}
