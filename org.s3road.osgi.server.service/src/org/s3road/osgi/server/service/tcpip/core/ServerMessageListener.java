/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *******************************************************************************/ 

package org.s3road.osgi.server.service.tcpip.core;

import org.s3road.osgi.server.AbstractMessage;


/**
 * This interface will be used as listener of the Server messages that are
 * sent to the corresponding nodes. 
 * The DENM/CAM message must be always an DENM/CAMMessageResponse. 
 */
public interface ServerMessageListener {

	/**
	 * Receives a message from the Server with the response to a SSAPMessageRequest
	 * or a subscription notification. This Response can be either a notification
	 * (SSAPMessageIndication)
	 * or a 
	 * @param message the response received from the Server
	 */
	public void messageReceived(AbstractMessage message);
}
