package org.s3road.osgi.server.service.tcpip.core;

import java.util.ArrayList;
import java.util.List;

import org.s3road.osgi.server.IServer;
import org.s3road.osgi.server.IServerStateListener;


public class ServerStateSupport {

	protected List<IServerStateListener> listeners = null;
	
	public ServerStateSupport() {
		listeners = new ArrayList<IServerStateListener>( );
	}
  
    public void addListener(IServerStateListener l) {
        if(l == null) {
            throw new IllegalArgumentException("The listener can not be null");
        }
        synchronized(listeners) {
        	listeners.add(l);
        }
    }

    public void removeListener(IServerStateListener l) {
    	synchronized(listeners) {
    		listeners.remove(l);
    	}
	}
    
    public void fireServerStopped(IServer server) {
    	synchronized(listeners) {
    		for(IServerStateListener listener : listeners) {
    			listener.serverStopped(server);
    		}
    	}
    }

    public void fireServerRunning(IServer server) {
    	synchronized(listeners) {
    		for(IServerStateListener listener : listeners) {
    			listener.serverRunning(server);
    		}
    	}
    }
}
