/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *******************************************************************************/ 

package org.s3road.osgi.server.service.tcpip.core;

import org.s3road.osgi.server.service.tcpip.listener.ConnectionListener;
import org.s3road.osgi.server.service.tcpip.warehouse.ListenerSupport;


/**
 * ClientConnectionSupport.java
 *
 */
public class ClientConnectionSupport extends ListenerSupport<ConnectionListener> {
	
	public ClientConnectionSupport() {
		super();
	}

	public void fireConnectionClosed(ClientConnection client) {
		synchronized(listeners) {
			for(ConnectionListener listener : listeners) {
				listener.connectionClosed(client);
			}
		}
	}

	public void fireConnectionError(int idError, String errorDescription, ClientConnection client) {
		synchronized(listeners) {
			for(ConnectionListener listener : listeners) {
				listener.connectionError(idError, errorDescription, client);
			}
		}
	}

	public void fireConnectionTimeout(ClientConnection client) {
		synchronized(listeners) {
			for(ConnectionListener listener : listeners) {
				listener.connectionTimeout(client);
			}
		}
	}

	public void fireMessageReceived(ClientMessage message, ClientConnection client) {
		synchronized(listeners) {
			for(ConnectionListener listener : listeners) {
				listener.messageReceived(message, client);
			}
		}
	}
	
}
