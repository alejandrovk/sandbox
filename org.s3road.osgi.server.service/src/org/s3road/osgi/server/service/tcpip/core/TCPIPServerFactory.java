/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *******************************************************************************/ 

package org.s3road.osgi.server.service.tcpip.core;

import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.s3road.osgi.server.IServer;
import org.s3road.osgi.server.IServerFactory;
import org.s3road.osgi.server.ServerException;
import org.s3road.osgi.server.service.ServerActivator;

/**
 * TCPIPManager.java
 * 
 * The TCP/IP manager responsible for creating and destroying tcp/ip gateways.
 */
public class TCPIPServerFactory implements IServerFactory, ServiceTrackerCustomizer<Object, Object> {

	/** The name of this Factory */
	private static final String TYPE = "TCP/IP";
	
	/** A list containing all gateway instances of this type */
	protected Map<Integer, IServer> instances;
	
	/** A list with the OSGi service registrations **/
	private Map<Integer, ServiceRegistration<?>> registrations;
	
	/** Declares a bundle context */
	private BundleContext bc;
	
	/**
	 * Constructor
	 * @param bc	The bundle context
	 */
	public TCPIPServerFactory(BundleContext bc) {
		this.bc = bc;
		this.instances = new HashMap<Integer, IServer>();
		this.registrations = new HashMap<Integer, ServiceRegistration<?>>();
	}
	
	@Override
	public Object addingService(ServiceReference<Object> reference) {
		// Nothing to do.
		return null;
	}
	
	@Override
	public void modifiedService(ServiceReference<Object> reference, Object serviceObject) {
		// Obtains the SIB
		Object obj = bc.getService(reference);
		
		if (obj instanceof IServer) {
			IServer server = (IServer) bc.getService(reference);
			ServerActivator.getLogger().log(LogService.LOG_INFO, "Added IServer service with name " + server.getName());
		}
	}

	@SuppressWarnings({ })
	@Override
	public void removedService(ServiceReference<Object> reference, Object service) {
		// Obtains the SIB
		Object obj = bc.getService(reference);
		
		if (obj instanceof IServer) {
			IServer server = (IServer) bc.getService(reference);
			ServerActivator.getLogger().log(LogService.LOG_INFO, "Removed IServer service with name " + server.getName());
//			Iterator<AbstractGateway> iterator = instances.values().iterator();
//			while(iterator.hasNext()) {
//				AbstractGateway ag = iterator.next();
//				if(ag.getSIB().equals(sib)) {
//					iterator.remove();
//					try {
//						ag.stop();
//					} catch(GatewayException ge) {
//						System.err.println("Could not stop gateway [" + ag.getName() + "]: " + ge.getMessage());
//					} catch(Exception e) {
//						e.printStackTrace();
//					}
//					ServiceRegistration sr = this.registrations.remove(ag.getId());
//					if(sr != null) {
//						sr.unregister();
//					}
//				}
//			}
		}
	}
	
	
	/**
	 * Creates a new instance of a server
	 * @param name				the name of the new server
	 * @param properties		the properties of this server.
	 * @return					a new server
	 * @throws GatewayException	if the server can not be created.
	 */
	@SuppressWarnings({ "rawtypes" })
	public int create(String name, Properties properties) throws ServerException {
		
		Dictionary<String, String> props = new Hashtable<String, String>();
		for(String key : properties.stringPropertyNames()) {
			  String value = properties.getProperty(key);
			  props.put(key, value);
		}
		
		// First create the instance.
		IServer server = createNewInstance(name, properties);
		
		// Then register it.
		ServiceRegistration service = bc.registerService(IServer.class.getName(), server, props);

		this.instances.put(server.getId(), server);
		this.registrations.put(server.getId(), service);

		return server.getId();
	}

	/**
	 * Destroys an instance of a server
	 * @param ag				the server reference to be destroyed.
	 * @throws ServerException	 if the gateway can not be destroyed.
	 */	
	@Override
	public void destroy(int id) throws ServerException {
		IServer ag = instances.remove(id);
		if(ag != null) {
			try {
				// Unregister from OSGi
				ServiceRegistration<?> sr = this.registrations.remove(ag.getId());
				if(sr != null) {
					sr.unregister();
				}
				// Stop the gateway.
				ag.stop();
				ag = null;
			} 
			catch(ServerException ge) {
				throw new ServerException(ge.getMessage(), ge);
			} 
			catch(Exception e) {
				throw new ServerException(e.getMessage(), e);
			}
		} 
		else {
			throw new ServerException("The gateway with id=" + id + "does no exist");
		}
	}
	
	/**
	 * Shutdowns the manager. This method stops and remove all gateway instances of the manager.
	 */
	public void shutdown() {
		// Unregister all from OSGi.
		for(ServiceRegistration<?> sr : registrations.values()) {
			sr.unregister();
		}
		
		// Stop all gateways.
		for(IServer ag: instances.values()) {
			try {
				ag.stop();
			} 
			catch(ServerException ge) {
				System.err.println("Could not stop gateway [" + ag.getName() + "]: " + ge.getMessage());
			} 
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		// Clear caches.
		this.instances.clear();
		this.registrations.clear();
	}
	
	@Override
	public IServer getServer(int id) {
		return this.instances.get(new Integer(id));
	}
	
	@Override
	public Collection<Integer> listServers() {
		return instances.keySet();
	}
	
	public IServer createNewInstance(String name, Properties properties) throws ServerException {
		try {
			if(checkPort(properties.get(TCPIPServerConfiguration.PORT))) {
				TCPIPServer server = new TCPIPServer(name, getType(), properties);
				ServerActivator.getLogger().log(LogService.LOG_INFO, "Created a new TCP/IP Server instance [" + server.getName() + "]");
				return server;
			} 
			else {
				throw new ServerException("Can not create a tcp/ip server instance.");
			}
		} 
		catch(Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}
	
	/**
	 * @return The default properties of the TCP/IP server.
	 */
	public Properties getConfigurableProperties() {
		return TCPIPServerConfiguration.getDefaultProperties();
	}
	
	
	private boolean checkPort(Object sport) throws ServerException {
		int port = TCPIPServerConfiguration.DEFAULT_PORT; 
		if(sport != null) {
			if(sport instanceof String) {
				try {
					port = Integer.parseInt((String)sport);
				} 
				catch(Exception e) {
					throw new NumberFormatException("Parameter 'PORT' is not valid");
				}
			} 
			else if(sport instanceof Integer) {
				port = (Integer)sport;
			} 
			else {
				throw new NumberFormatException("Parameter 'PORT' is not valid");
			}
		}
		// Check if there is another gateway running in the same port.
		for(IServer server : instances.values()) {
			TCPIPServer tserver = (TCPIPServer)server;
			if(port == tserver.getConfig().getPort()) {
				throw new ServerException("Port " + port + " is already used by gateway " + server.getName());
			}
		}

		return true;
	}

	@Override
	public String getType() {
		return TYPE;
	}
}
