/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia Research and Innovation - Software Systems Engineering) - initial API, implementation and documentation
 *******************************************************************************/ 

package org.s3road.osgi.server.service.tcpip.parser;

@SuppressWarnings("serial")
public class MessageParseException extends Exception {

	/**
	 * Constructs a new exception with <code>null</code> as its detail message. 
	 * The cause is not initialized, and may subsequently be initialized by a 
	 * call to {@link Throwable.initCause(java.lang.Throwable)}.
	 * @param message the detail message. The detail message is saved for later retrieval by the Throwable.getMessage() method.
	 */
	public MessageParseException() {
		super();
	}

		
	/**
	 * Constructs a new exception with the specified detail message. 
	 * The cause is not initialized, and may subsequently be initialized by a 
	 * call to {@link Throwable.initCause(java.lang.Throwable)}.
	 * @param message the detail message. The detail message is saved for later retrieval by the Throwable.getMessage() method.
	 */
	public MessageParseException(String message) {
		super(message);
	}
		
	/**
	 * Constructs a new exception with the specified detail message and cause.<br/>
	 * Note that the detail message associated with cause is not automatically incorporated in this exception's detail message. 
	 * @param message the detail message. The detail message is saved for later retrieval by the Throwable.getMessage() method.
	 * @param cause
	 */
	public MessageParseException(String message, Throwable cause) {
		super(message,cause);
	}
}
