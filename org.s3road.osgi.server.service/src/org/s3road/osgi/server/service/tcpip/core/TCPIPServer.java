/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *******************************************************************************/ 

package org.s3road.osgi.server.service.tcpip.core;


import java.util.HashMap;
import java.util.Properties;

import org.osgi.service.log.LogService;
import org.s3road.osgi.server.IServer;
import org.s3road.osgi.server.IServerStateListener;
import org.s3road.osgi.server.ServerException;
import org.s3road.osgi.server.service.ServerActivator;
import org.s3road.osgi.server.udp.core.MultiCastWriter;




/**
 * TCPIPServer.java
 * 
 * Handles reading from clients and hands off events to the consumer.
 * 
 */
public class TCPIPServer implements IConnectionListener, IServer {
	
	/**
	 * Types of possible transactions
	 */
	public enum ServerStatus {  
		RUNNING		(1),
		STOPPED		(0);
		
		private final int status;
		
		ServerStatus(int status) {
			this.status = status;
		}
		
		public int getStatus() {
			return status;
		}
		
		public boolean isRunning() {
			return (this == ServerStatus.RUNNING);
		}
		
		public boolean isStopped() {
			return (this == ServerStatus.STOPPED);
		}
	}	
	
	/** The generator id of the server */
	private static int idGenerator;
	
	/** A latch to avoid concurrency while creating a new id */
	private static Object uniqueId;
	
	/** The hash map containing all sessions of this server */
	protected HashMap<Long, Session> sessions;

	/** A support class to manage IServerStateListeners */
	private ServerStateSupport stateSupport;
	
	/** The id of the server */
	protected int id;
	
	/** The name of the server */
	protected String name;
	
	/** The state of the server RUNNING or STOPPED */
	private ServerStatus status;
	
	/** The type of the gateway */
	private String type;
	
	static {
		uniqueId = new Object();
	}
	
	/**
	 * The properties defining this gateway 
	 */
	protected Properties properties;
	
	
	/**
	 * This class is responsible for accepting incoming client connections.
	 */
	private SocketChannelAcceptor channelAcceptor;
	/**
	 * This class is responsible for reading the information from the client channels.
	 */
	private SocketChannelReader channelReader;

	/**
	 * This class is responsible for writing the information to client channels.
	 */
	private SocketChannelWriter channelWriter;
	
	/**
	 * Writes multicast UDP messages to connected clients
	 */
	private MultiCastWriter multicastWriter;

	/** 
	 * The ThreadGroup where all threads related with TCP/IP gateway will run.
	 */
	private ThreadGroup tg;

	/**
	 * The configuration of the tcp/ip gateway.
	 */
	private TCPIPServerConfiguration config;
	
	/**
	 * The logger
	 */
	
	private volatile boolean running;
	
	/**
	 * Constructs a new gateway.
	 * 
	 * @param name				the name for the new gateway.
	 * @param sib				the sib this gateway is attached to. 
	 * @param properties		the specific properties to create a concrete. 
	 * @throws GatewayException
	 * @see GatewayException
	 */
	public TCPIPServer(String name, String type, Properties properties) {
		this.name = name;
		this.type = type;
		this.properties = properties;
		this.id = generateId(); 
		this.sessions = new HashMap<Long,Session>();
		this.stateSupport = new ServerStateSupport();
		this.status = ServerStatus.STOPPED;
		this.config = new TCPIPServerConfiguration();
		config.setProperties(properties);
		
		// Create the ThreadGroup
		tg = new ThreadGroup("TCP/IP Server");
	}
		
	/**
	 * Adds a listener to see server state changes (running, stopped).
	 */
	@Override
	public void addStateListener(IServerStateListener listener) {
		this.stateSupport.addListener(listener);
	}

	/**
	 * Removes a listener to see server state changes (running, stopped).
	 */
	@Override
	public void removeStateListener(IServerStateListener listener) {
		this.stateSupport.removeListener(listener);
	}
	
	
	/** 
	 * Generates a unique id per server
	 * @return a unique id
	 */
	private static int generateId() {
		synchronized(uniqueId) {
			return ++idGenerator;
		}
	}

	/**
	 * This method starts the server. 
	 */
	public void startServer() throws ServerException {
		try {
			ServerActivator.getLogger().log(LogService.LOG_INFO, "Starting the TCP/IP server...");
			channelWriter= new SocketChannelWriter(tg, config);
			channelWriter.start();
			
			channelReader = new SocketChannelReader(tg, config);
			channelReader.setConnectionListener(this);
			channelReader.start();
			
			channelAcceptor = new SocketChannelAcceptor(tg, channelReader, channelWriter, config);
			channelAcceptor.start();
			
			multicastWriter = new MultiCastWriter("MultiCastWriter");
			multicastWriter.start();
			
			running = true;
		} 
		catch(Exception e) {
			ServerActivator.getLogger().log(LogService.LOG_ERROR, "Could not start the TCPIPServer. Reason is " + e.getMessage());
			try {
				stop();
				running = false;
				throw new ServerException(e.getMessage());
			} 
			catch(Exception e2) {
				running = false;
				throw new ServerException(e.getMessage(), e2);
			}
			
		}
	}

	/**
	 * Shutdown the server.
	 */
	public void stopServer() throws ServerException {
		if(!running) {
			return;
		}
		ServerActivator.getLogger().log(LogService.LOG_INFO, "Stopping the TCP/IP server...");
		try {
			if(channelAcceptor != null) {
				channelAcceptor.shutdown();
				channelAcceptor = null;
			}
			if(channelReader != null) {
				channelReader.shutdown();
				channelReader = null;
			}
			if(channelWriter != null) {
				channelWriter.shutdown();
				channelWriter = null;
			}
			
			if (multicastWriter != null){
				if (multicastWriter.shutdown()){
					multicastWriter = null;
				}
				else{
					ServerActivator.getLogger().log(LogService.LOG_ERROR, "Could not stop the MultiCastWriter.");
				}
				
			}

			ServerActivator.getLogger().log(LogService.LOG_INFO, "TCPIPServer stopped");
		} 
		catch(Exception e) {
			ServerActivator.getLogger().log(LogService.LOG_ERROR, "Could not stop the TCPIPServer. Reason is " + e.getMessage());
			throw new ServerException(e.getMessage(), e);
		}
	}

	/**
	 * @return The configuration of the TCP/IP server
	 */
	public TCPIPServerConfiguration getConfig() {
		return this.config;
	}

	public MultiCastWriter getMulticastWriter() {
		return multicastWriter;
	}

	public void setMulticastWriter(MultiCastWriter multicastWriter) {
		this.multicastWriter = multicastWriter;
	}

	@Override
	public void connectionCreated(ClientConnection connection) {
		Session session = new Session();
		connection.setServer(this);
		session.setServer(this);
		session.setConnector(connection);
		this.addSession(session);
	}
	
	@Override
	public void connectionDestroyed(ClientConnection connection) {
		Session sessionToRemove = null;
		for(Session session : this.sessions.values()) {
			ClientConnection cc = (ClientConnection )session.getConnector(); 
			if(cc == connection) {
				sessionToRemove = session;
				break;
			}
		}
		if(sessionToRemove != null) {
			this.removeSession(sessionToRemove);
		}
	}
	
	/**
	 * Adds a session to the Session manager
	 *  
	 * @param session the session to be added
	 * @see Session
	 */
	public void addSession(Session session) {
		sessions.put(session.getId(), session);
	}
	
	/**
	 * Removes a session from the session
	 *  
	 * @param session 	the session to be removed
	 * @return the session removed or <code>null</code> if the session does not exist.
	 */
	public Session removeSession(Session session) {
		return sessions.remove(session.getId());
	}

	/**
	 * Gets the name of the gateway.
	 * @return the name of the gateway
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the properties of this gateway.
	 * @return The properties associated to the gateway
	 */
	public Properties getProperties() {
		return properties;
	}

	/**
	 * Starts the server
	 * @throws ServerException if the gateway can not be started.
	 * @see ServerException
	 */
	public void start() throws ServerException {
		startServer();
		this.status = ServerStatus.RUNNING;
		this.stateSupport.fireServerRunning(this);
	}

	/**
	 * Stops the server
	 * @throws ServerException if the gateway can not be stopped.
	 * @see ServerException
	 */
	public void stop() throws ServerException {
		for(Session session : sessions.values()) {
			session.close();
		}

		stopServer();
		this.status = ServerStatus.STOPPED;
		this.stateSupport.fireServerStopped(this);
	}
	
	/**
	 * Gets the state of the gateway: RUNNING or STOPPED.
	 */
	public int getStatus() {
		return this.status.getStatus();
	}

	/**
	 * Gets if the gateway is running
	 * @return <code>true</code> if the gateway is running
	 */
	public boolean isRunning() {
		return this.status.isRunning();
	}	

	/**
	 * Gets if the server is stopped
	 * @return <code>true</code> if the gateway is stopped
	 */
	public boolean isStopped() {
		return this.status.isStopped();
	}		
	
	/**
	 * Gets the type of the server.
	 */
	public String getType() {
		return this.type;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public IServer getServer() {
		return this;
	}
	
}
