/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)  
 *******************************************************************************/ 

package org.s3road.osgi.server.service.tcpip.core;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.osgi.service.log.LogService;
import org.s3road.osgi.server.AbstractMessage;
import org.s3road.osgi.server.IConnector;
import org.s3road.osgi.server.IServer;
import org.s3road.osgi.server.IServerCommunication;
import org.s3road.osgi.server.ServerException;
import org.s3road.osgi.server.service.ServerActivator;
import org.s3road.osgi.server.service.tcpip.parser.DENMMessage;
import org.s3road.osgi.server.service.tcpip.parser.Message;
import org.s3road.osgi.server.service.tcpip.parser.MessageParseException;
import org.s3road.osgi.server.service.tcpip.parser.MessageParser;


/**
 * The connector that stores the reference of the internal connection of the KP
 * as well as possible errors that can arise during the presentation.
 *
 */
public abstract class Connector implements IConnector {

	/** The session reference */
	private IServerCommunication session;

	/** Indicates if the connection is persistent */
	private boolean persistent;
	
	/**
	 * A reference to the Server
	 */
	protected IServer server;
	
	public abstract void close();
	
	/**
	 * Establish if this connection is persistent.
	 * @param persistent
	 * 		Indicates if this connection is persistent. If so, it will notify to session 
	 * 		when the connection is closed, timeout or error. 
	 */
	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}
	
	/**
	 * Gets if the connection is persistent or not.
	 * @return <code>yes</code> if it is persistent, <code>false</code> otherwise.
	 */
	public boolean isPersistent() {
		return this.persistent;
	}
	
	@Override
	/**
	 * This is the method that the connector must invoke when a new message represented
	 * in a byte array arrives.
	 */
	public void fireMessageReceived(byte[] stream) {
		if(session != null) {
			//TODO: Get the speed and create a DENMessage to send it
			try {
				String decoded = new String(stream, "UTF-8");
				if (decoded.contains("Speed")){
					Message message = MessageParser.parse(stream);
					String payload = message.getPayload();
					if (!payload.isEmpty()){
						//get the speed from the payload
						String speedString = payload.substring(payload.lastIndexOf(":") + 1, payload.length());
						short speed = Short.parseShort(speedString.trim());
						DENMMessage dmessage = new DENMMessage();
						StringBuilder builder = new StringBuilder();
						builder.append("New speed is " + speed);
						dmessage.setDenmDistance(speed);
						//Get the current time and set it on Creation time
						DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						Date date = new Date();
						ServerActivator.getLogger().log(LogService.LOG_INFO, "Current date for DENMMessage is "  + dateFormat.format(date));
						long currentTime = date.getTime();
						dmessage.setCreationTime(currentTime);
						if (server != null){
							((TCPIPServer)server).getMulticastWriter().addIncomingMessage(dmessage);
						}
						
					}
					
				}
			} 
			catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MessageParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			try {
////				System.out.println("Gateway parsing:\n" + new String(stream));
//				request = SSAPMessageParser.parse(stream);
////				System.out.println("Parsed:\n" + request.toString());
//				session.sendToSIB(request);
//			} catch (SSAPMessageParseException e) {
//				System.err.println("Error while parsing:\n" + new String(stream));
//				e.printStackTrace();
//				//TODO [ROM] Send and invalid SSAPMessageFormat to the client.
//			}
		}
	}

	/**
	 * Gets the received message from the SIB and pass it to the connector in order
	 * to send it to the KP.
	 */
	@Override
	public void messageReceivedFromSIB(AbstractMessage response) {
		sendToKP(response.toByteArray());
	}

	/**
	 * Fires a connection timeout when the connection is closed
	 */
	@Override
	public void fireConnectionClosed() {
		if(session != null) {
			session.connectionClosed();
		}
	}

	/**
	 * Fires a connection timeout to 
	 * @param ex a ServerException 
	 */
	@Override
	public void fireConnectionError(ServerException ex) {
		if(session != null) {
			session.connectionError(ex.getMessage());
		}
	}

	/**
	 * Fires a connection timeout when no response is received from the KP
	 */
	@Override
	public void fireConnectionTimeout() {
		if(session != null) {
			session.connectionTimeout();
		}
	}

	/**
	 * Associates the connection to a SIBSession
	 * @param sibSession a SIBSession
	 */
	public void setSIBSession(Session session) {
		this.session = session;
	}
	
}
