/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *******************************************************************************/

package org.s3road.osgi.server.udp.core;

import java.io.*;
import java.net.*;
import java.util.concurrent.ArrayBlockingQueue;

import org.osgi.service.log.LogService;
import org.s3road.osgi.server.service.ServerActivator;
import org.s3road.osgi.server.service.tcpip.parser.DENMMessage;

/**
 * Class that implements a Multicast message writer, used to send UDP datagrams with DENM/CAM messages to connected clients
 * @author Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *
 */
public class MultiCastWriter extends Thread {

 
	/**
	 * The queue containing the messages
	 */
	private ArrayBlockingQueue<DENMMessage> queue;
	
	/**
	 * A thread-blocking lock
	 */
	private Object lock;
	
	/**
	 * The socket to send UDP messages
	 */
	private DatagramSocket socket = null;
    
	/**
	 * Thread control flag
	 */
    private boolean running = true;
    
    /**
     * Waiting intervale after each message is sent
     */
    private final long FIVE_SECONDS = 5000;
    
    /**
     * The multicast port to be used
     */
    private final int MULTICAST_PORT = 23005;
    
    /**
     * The multicast address to be used
     */
    private final String MULTICAST_ADDRESS = "192.168.1.255";
    
    /**
     * The queue maximum size
     */
    private final int WRITE_QUEUE_SIZE = 1024;

    /**
     * Constructor
     * @throws IOException
     */
    public MultiCastWriter() throws IOException {
        this("MulticastServerThread");

    }
    
    /**
     * Constructor
     * @param name	The class name
     * @throws IOException
     */
    public MultiCastWriter(String name) throws IOException {
    	super(name);
        socket = new DatagramSocket(MULTICAST_PORT);
        socket.setBroadcast(true);
        this.lock = new Object();
        queue = new ArrayBlockingQueue<DENMMessage>(WRITE_QUEUE_SIZE);
    }

    
    /**
     * Run method. Waits for the queue to be populated and the sends the messages 
     */
    public void run() {
    	ServerActivator.getLogger().log(LogService.LOG_INFO, "Starting the MultiCastWriter thread...");
    	while (running) {
            try {
                //wait until there are some elements in the queue
            	if(queue.size() == 0) {
					synchronized(lock) {
						lock.wait();
					}
				}
            	if(queue.size() > 0) {
            		
    				while (queue.size() > 0) {
    					DENMMessage message = queue.take();
    					
    					// send it
    	                InetAddress group = InetAddress.getByName(MULTICAST_ADDRESS);
    	                DatagramPacket packet = new DatagramPacket(message.toByteArray(), message.toByteArray().length, group, MULTICAST_PORT);
    	                socket.send(packet);

    	                // sleep for a while
    	                try {
    	                	sleep((long)(Math.random() * FIVE_SECONDS));
    	                } 
    	                catch (InterruptedException e) { 
    	                	
    	                }
    					
    				}
            	}
            } 
            catch (IOException e) {
            	ServerActivator.getLogger().log(LogService.LOG_ERROR, "An error occured trying to send a Multicast message. Reason is " + e.getMessage());
                running = false;
            }
            catch (InterruptedException ex) {
            	ServerActivator.getLogger().log(LogService.LOG_ERROR, "An error occured trying to send a Multicast message. Reason is " + ex.getMessage());
                running = false;
            }
        }
    	socket.close();
    }
    
	/**
	 * Shutdown the thread if the queue is empty only
	 */
	public boolean shutdown() {
		if (queue.isEmpty()){
			ServerActivator.getLogger().log(LogService.LOG_INFO, "Shutting down MultiCastWriter...");
			running = false;
			this.interrupt();
			ServerActivator.getLogger().log(LogService.LOG_INFO, "MultiCastWriter stopped");
			return true;
		}
		else{
			ServerActivator.getLogger().log(LogService.LOG_ERROR, "Could not shutdown MultiCastWriter thread");
			return false;
		}
	}
	
	/**
	 * Adds a message to the messages queue. 
	 * 
	 * @param 	message 	The DENMMessage object that contains the data
	 * @return	None
	 */
	public void addIncomingMessage(DENMMessage message) {
		queue.add(message);
		//Notify the waiting threads
		synchronized(lock) {
			lock.notifyAll();
		}
	}
}
