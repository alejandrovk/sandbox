/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *******************************************************************************/ 
 
package org.s3road.osgi.server;

/**
 * The interface that defines the set of methods to be implemented by the connector.
 * 

 */
public interface IConnector {

	/**
	 * Notifies that the connector receives data from a Knowledge Processor.
	 * @param stream an array of bytes
	 */
	public void fireMessageReceived(byte[] stream);
	
	/**
	 * Sets the TCPIPServer
	 */
	public void setServer(IServer server);
	
	/**
	 * Receives a Message response from the Server.
	 * @param response
	 */
	public void messageReceivedFromSIB(AbstractMessage response);

	/**
	 * Fires a connection error to ...
	 * @param ex a ServerException 
	 */
	public void fireConnectionError(ServerException ex);	

	/**
	 * Fires a connection timeout to ...
	 */
	public void fireConnectionTimeout();

	/**
	 * Fires a connection closed error to ...
	 */
	public void fireConnectionClosed();
	
	/**
	 * Send to KP a byte array SSAP message.
	 * @param a byte array representing a SSAP xml message.
	 */
	public void sendToKP(byte[] ba);
}
