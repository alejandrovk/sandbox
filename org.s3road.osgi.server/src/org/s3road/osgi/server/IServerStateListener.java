package org.s3road.osgi.server;

public interface IServerStateListener {
	public void serverStopped(IServer server);
	public void serverRunning(IServer gw);
}
