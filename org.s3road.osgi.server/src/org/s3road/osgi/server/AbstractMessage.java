/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *  Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)  
 *******************************************************************************/ 

package org.s3road.osgi.server;

import java.nio.charset.Charset;


/**
 * This abstract class represents an abstract Message
 * 
 * @author Alejandro Villamarin alejandro.villamarin@tecnalia.com
 */
public abstract class AbstractMessage {

	/**
	 * Used to represent the DENM / CAMN protocol version - 1 byte
	 */
	protected byte version;
	
	/**
	 * The client Name that is generating the message
	 */
	protected String clientName;
		
	/**
	 * Used to represent the message id - 1 byte
	 */
	protected byte messageId;

	/**
	 * Message creation time, maximum 6 bytes long
	 */
	protected long creationTime;
	
		
	/**
	 * Public constructor
	 */
	public AbstractMessage() {
	}


	protected byte getVersion() {
		return version;
	}

	protected byte getMessageId() {
		return messageId;
	}

	protected long getCreationTime() {
		return creationTime;
	}

	protected String getClientName() {
		return clientName;
	}
	
	public void setVersion(byte version) {
		this.version = version;
	}


	public void setMessageId(byte messageId) {
		this.messageId = messageId;
	}


	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}
	
	protected void setClientName(String clientName) {
		this.clientName = clientName;
	}


	/**
	 * Gets the byte[] representation of the Message
	 * @return a byte[] array
	 */
	public byte[] toByteArray() {
		if (Charset.isSupported("UTF-8")) {
			return toString().getBytes(Charset.forName("UTF-8"));
		}
		else {
			return toString().getBytes();
		}
	}
}
