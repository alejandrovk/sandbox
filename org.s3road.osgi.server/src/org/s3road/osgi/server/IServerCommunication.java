/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)  
 *******************************************************************************/ 

package org.s3road.osgi.server;

/**
 * The interface that defines the set of methods to be implemented by the connector.
 * 
 */
public interface IServerCommunication {

	/**
	 * Sends an Message to the Server
	 * @param request a MessageRequest
	 */
	public void sendToServer(AbstractMessage request);
	
	/**
	 * Notifies about a connection error.
	 * @param errorMsg the message associated to the error
	 */
	public void connectionError(String errorMsg);

	/**
	 * Notifies about a connection error.
	 */
	public void connectionTimeout();	

	/**
	 * Notifies about a connection error.
	 */
	public void connectionClosed();

	void messageReceived(AbstractMessage message);

}
