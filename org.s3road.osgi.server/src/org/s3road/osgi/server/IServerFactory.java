/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *******************************************************************************/ 

package org.s3road.osgi.server;

import java.util.Collection;
import java.util.Properties;



/**
 * This interface must be implemented by the server managers in order to manage them. 

 */
public interface IServerFactory {
	/**
	 * Creates a server associated
	 * @param name the server name
	 * @param properties the properties associated to the server
	 * @return the identifier of created server
	 * @throws ServerException
	 */
	public int create(String name, Properties properties) throws ServerException;
	
	/**
	 * Destroys a server identified by the parameter <code>id</code>
	 * @param id the server identifier
	 * @throws ServerException when cannot destroy the server
	 */
	public void destroy(int id) throws ServerException;
	
	/**
	 * Gets the server type
	 * @return the server type
	 */
	public String getType();
	
	/**
	 * Gets the default properties associated to the server type
	 * @return default properties
	 */
	public Properties getConfigurableProperties();
	
	/**
	 * Gets the server identified by the parameter
	 * @param id the server identifier
	 * @return the IServer associated to the server
	 */
	public IServer getServer(int id);
	
	/**
	 * Gets the list of available servers 
	 * @return the list of server
	 */
	public Collection<Integer> listServers();
}
