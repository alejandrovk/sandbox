/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Author:
 *    Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *    
 *******************************************************************************/ 
package org.s3road.osgi.server;

import java.util.Properties;


/**
 * Defines the public contract for the S3Road OSGi server
 * @author Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *
 */
public interface IServer {
	
	/**
	 * Starts a server
	 * @throws ServerException
	 */
	public void start() throws ServerException;

	/**
	 * Stops a gateway
	 * @throws ServerException
	 */
	public void stop() throws ServerException;
	
	/**
	 * Retrieves the id of the server
	 */
	public int getId();
	
	/**
	 * Retrieves the state of a server (STOPPED or RUNNING)
	 */
	public int getStatus();
	
	/**
	 * Gets if the server is running
	 * @return <code>true</code> if the server is running
	 */
	public boolean isRunning();
	
	/**
	 * Gets if the server is stopped
	 * @return <code>true</code> if the server is stopped
	 */
	public boolean isStopped();	
	
	/**
	 * Retrieves the server
	 */
	public IServer getServer();
	
	/**
	 * Adds a listener to see server state changes (running, stopped).
	 */
	public void addStateListener(IServerStateListener listener);		

	/**
	 * Removes a listener to see gateway state changes (running, stopped).
	 */
	public void removeStateListener(IServerStateListener listener);		
	
	/**
	 * Retrieves the name of the server
	 */
	public String getName();

	/**
	 * Retrieves the type of the server.
	 */
	public String getType();
	
	/**
	 * Retrieves the Properties.
	 */
	public Properties getProperties();
	
}
