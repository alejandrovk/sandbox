/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Alejandro Villamarin alejandro.villamarin@tecnalia.com (Tecnalia Research and Innovation)
 *******************************************************************************/ 

package org.s3road.osgi.server;

/**
 * The exceptions throwed inside the server
 * 
 */
@SuppressWarnings("serial")
public class ServerException extends Exception {

	public ServerException() { 
		super(); 
	}
	
	public ServerException(String message) { 
		super(message, null); 
	}

	public ServerException(String message, Throwable cause) { 
		super(message, cause); 
	}

}
