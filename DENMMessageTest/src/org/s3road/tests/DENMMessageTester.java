package org.s3road.tests;

import org.s3road.reader.message.DENMMessage;

public class DENMMessageTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a message from scratch
		Long time = System.currentTimeMillis();
		DENMMessage mes = new DENMMessage();
		mes.setCreationTime(time);
		mes.setExpirationDate(time + 50000);
		mes.setProtocolversion((short) 200);
		mes.setNegated(true);
		mes.setReliability((short) 100);
		mes.setStationId(67300);
		mes.setSeqNumber((short) 3);
		
		mes.setCause((short) 7);
		mes.setSubcause((short) 0);
		
		byte [] bytes = mes.toByteArray();
		
		System.out.println("Generated DENM message. Length: " + bytes.length);
		System.out.println("Hexadecimal:\n" + bytesToHex(bytes));
		
		// Create a blank message:
		DENMMessage mes2 = new DENMMessage();
		
		System.out.println("Blank message. Length: " + mes2.toByteArray().length);
		System.out.println("Hexadecimal:\n" + bytesToHex(mes2.toByteArray()));
		
		// Regeneration of the message
		mes = null;
		mes = new DENMMessage(bytes);
		
		System.out.println("Regenerated DENM Message:\n" + mes.toString());

	}
	
	final protected static char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for ( int j = 0; j < bytes.length; j++ ) {
	        v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}

}
