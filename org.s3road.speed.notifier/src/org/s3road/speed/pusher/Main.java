package org.s3road.speed.pusher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.smool.kpi.common.Logger;

/**
 * Main entry point class
 * @author Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *
 */
public class Main implements Runnable{
	
	/**
	 * The thread object
	 */
	private Thread thread;
	
	/**
	 * Control flag
	 */
	private boolean running;
	
	/**
	 * Reference to the pusher object
	 */
	private Pusher pusher;
	
	/**
	 * Used to validate IPv4 address
	 */
	private static final String PATTERN = 
	        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	/**
	 * Constructor
	 */
	public Main(){
		
	}
	
	public Pusher getPusher() {
		return pusher;
	}

	public void setPusher(Pusher pusher) {
		this.pusher = pusher;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Set up the logging system
		Logger.setDebugging(true);
		Logger.setClassNameActivated(true);
		Logger.setDatetimeActivated(true);
		Logger.setDebugLevel(4);
		
		Main object = new Main();
		
		int length = args.length;
        if (length <= 0) {
            Logger.error("ERROR: No arguments provided");
            showHelp();
            System.exit(1);
        }
        if (length == 3){  
        	try {
        		int port = Integer.parseInt(args[0]);
                String address = args[1];
                //validate correct IP address
                if (validate(address)){
                	
                	int interval;
					try {
						interval = Integer.parseInt(args[2]);
						Pusher pusher = new Pusher(port, address, interval);
						object.setPusher(pusher);
						object.getPusher().start();
						object.start();
					} 
					catch (NumberFormatException e) {
						Logger.error("INTERVAL argurment must be an integer");
		                showHelp();
		                System.exit(1);
					}
                	catch (IOException e) {
                		System.exit(1);
                	}
                }
                else{
                    Logger.error("Provided address must be in valid IPv4 format");
                    showHelp();
                    System.exit(1);
                }
            } 
            catch (NumberFormatException e) {
                Logger.error("PORT argurment must be an integer");
                showHelp();
                System.exit(1);
            }
        }
        else{
        	Logger.error("Incorrect number of arguments provided");
        	showHelp();
        	System.exit(1);
        }			
	}
	
	
	private static void showHelp() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("USAGE INSTRUCTIONS" + "\n");
		buffer.append("*******************************************" + "\n");
		buffer.append("You must provide a port and addres where the application will be sending speed messages." + "\n\n");
		buffer.append("You also need to specify the interval at which those messages are sent." + "\n\n");
		buffer.append("For instance: java -jar notifier.jar 12345 192.168.1.126 30" + "\n\n");
		buffer.append("Above example means this app will send messages to a server with IP 192.168.1.126, listening in port 12345 with an interval of 30 secons" + "\n\n");
		buffer.append("Thus, first argument is the PORT and second is the ADDRESS." + "\n\n");
		buffer.append("PORT is a [d]+ numeric field and the ADDRESS is [d]+.[d]+.[d]+.[d]+ format" + "\n\n");
		buffer.append("The interval perios is in [d]+ format, in seconds" + "\n\n");
		buffer.append("You must provide the three arguments for this app to work" + "\n\n");
		Logger.info(buffer.toString());
		
	}
	
	/**
	 * Checks if a provided String is an IPv4 address
	 * @param ip	The String
	 * @return	TRUE if String is IPv4, false otherwise
	 */
	public static boolean validate(final String ip){          

	      Pattern pattern = Pattern.compile(PATTERN);
	      Matcher matcher = pattern.matcher(ip);
	      return matcher.matches();             
	}
	
	/**
     * Starts the thread
     */
    public void start(){
    	// start Main thread
    	this.thread = new Thread(this, "Main");
    	this.thread.start();
    }
	
    
	@Override
	public void run() {
		running = true;
		Logger.debug(this.getClass().getName(), "Main Thread starting");
		
		while(running) {
			
			//ask for input and if the user enters something stop all threads and exit the app
			Logger.info("Press ENTER to kill the application...");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				br.readLine();
				//close the threads and terminate the app
				Logger.info("Gracefully terminating the app");
				pusher.destroy();
				running = false;
			} 
			catch (IOException e) {
				Logger.error("An error ocurred trying to get user input. Finishing thread and closing app...");
				pusher.destroy();
				running = false;
			}
			
		}
		Logger.debug(this.getClass().getName(), "Main Thread stopped");
	}
	
	/**
	 * Used to gracefully stop the thread
	 * 
	 * @return	None
	 */
	public void destroy() {
		//set control flag
		running = false;
		thread = null;
	}

}
