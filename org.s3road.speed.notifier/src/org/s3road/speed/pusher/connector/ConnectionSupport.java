/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *******************************************************************************/ 

package org.s3road.speed.pusher.connector;

import org.smool.kpi.common.AbstractSupport;

public class ConnectionSupport extends AbstractSupport<IConnectionListener> {
	
	public ConnectionSupport() {
		super();
	}
  
	public void fireConnectionClosed(AbstractConnector connector) {
		synchronized(listeners) {
			for(IConnectionListener listener : listeners) {
				listener.connectionClosed(connector);
			}
		}
	}

	public void fireConnectionError(AbstractConnector connector, String error) {
		synchronized(listeners) {
			for(IConnectionListener listener : listeners) {
				listener.connectionError(connector, error);
			}
		}
	}

	public void fireConnectionTimeout(AbstractConnector connector) {
		synchronized(listeners) {
			for(IConnectionListener listener : listeners) {
				listener.connectionTimeout(connector);
			}
		}
	}

}
