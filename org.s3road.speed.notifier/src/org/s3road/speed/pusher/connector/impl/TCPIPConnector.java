/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *******************************************************************************/ 

package org.s3road.speed.pusher.connector.impl;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SocketChannel;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import org.s3road.speed.pusher.connector.AbstractConnector;
import org.s3road.speed.pusher.connector.ClientConnectorException;
import org.s3road.speed.pusher.connector.ConnectionSupport;
import org.smool.kpi.common.Logger;



public class TCPIPConnector extends AbstractConnector implements Runnable {

	private static final String HOST = "HOST";
	private static final String DEFAULT_IPADDRESS = "DEFAULT_IPADDRESS";
	private static final String PORT = "PORT";
	private static final String KEEP_ALIVE = "KEEP_ALIVE";

	/**
	 * The thread object
	 */
	private Thread thread;
	
	/** Connection to server */
	private SocketChannel channel;

	/** Queue for incoming events */
	private LinkedBlockingQueue<ClientMessage> inQueue;
	
	/** Queue for outgoing events */
	private LinkedBlockingQueue<ClientMessage> outQueue;

	/** Lock to avoid expensive run() */
	private Object lock;
	
	/** Reference to NIOEventReader that reads events from the server */
	private SocketChannelReader reader;
	
	/** Buffer for outgoing events */
	private ByteBuffer writeBuffer;

	/** Still running? */
	private boolean running;
	
    public TCPIPConnector() throws IOException {
    	super();
		this.inQueue = new LinkedBlockingQueue<ClientMessage>();
		this.outQueue = new LinkedBlockingQueue<ClientMessage>();
		this.writeBuffer = ByteBuffer.allocate(TCPIPConfiguration.BUFFER_SIZE);
		this.lock = new Object();
		
		// start the reader (without a channel)
		this.reader = new SocketChannelReader(this);
		
		// start processing incoming/outgoing messages.
		this.thread = new Thread(this, "TCPIPConnector");
		this.thread.start();
    }
    
    
	/** 
	 * This is the threadeable method. Checks if there are pending messages in the output and input queues and
	 * process them. Waiting condition is whether any of the queues have at least a message
	 * 
	 * @return	None
	 */
	public void run() {
		running = true;
		while(running) {
			try {
				if(inQueue.size() == 0 && outQueue.size() == 0) {
					synchronized(lock) {
						lock.wait();
					}
				}
				//Check if we have to reconnect, only when KEEP_ALIVE is set to false
				if (!this.keepAlive){
					connect();
				}
				processIncomingMessages();
				writeOutgoingMessages();
				//Ask if we should close the connection once the queue has been processed
				//Check if we have to reconnect, only when KEEP_ALIVE is set to false
				if (!this.keepAlive){
					disconnect();
				}
				
			} catch (ClientConnectorException cex) {
				cex.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Logger.debug(this.getClass().getName(), "Client TCP/IP connector ends");
	}

	/** 
	 * Handle incoming messages from the inQueue.
	 * 
	 * @return	None
	 * 
	 */
	protected void processIncomingMessages() throws ClientConnectorException {
		try {
			if(inQueue.size() > 0) {
			
				ClientMessage msg;
				while (inQueue.size() > 0) {
					msg = inQueue.take();
					Logger.debug(this.getClass().getName(), "Received: " + msg);
					this.messageListeners.fireMessageReceived(msg.getPayload());
				}	

			}
		} catch(InterruptedException iex) {
			iex.printStackTrace();
			throw new ClientConnectorException("The socket is interrupted: " + iex.getMessage(), iex);
		}
	}
	
	
	/** 
	 * Write all events waiting in the outQueue.
	 * 
	 * @return	None
	 * 
	 */
	private void writeOutgoingMessages() throws ClientConnectorException {
		try {
			
			ClientMessage msg;
			if(outQueue.size() > 0) {
		
				while (outQueue.size() > 0) {
					msg = outQueue.take();
					long time = System.currentTimeMillis();
					writeMessage(msg);

					Logger.debug(this.getClass().getName(), new StringBuilder("Sent message in ")
						.append((System.currentTimeMillis()-time))
						.append(" ms: ")
						.append(msg).toString());
				}
			}
		} catch (ClientConnectorException cex) {
			throw new ClientConnectorException("Cannot write an outgoing message: " + cex.getMessage(), cex);
		} catch (InterruptedException iex) {
			throw new ClientConnectorException("The socket is interrupted: " + iex.getMessage(), iex);
		}
	}

	/**
	 * Connects to a SIB using TCP/IP
	 * 
	 * @return	None
	 */
	public synchronized void connect() throws ClientConnectorException {
		if(this.channel != null && this.channel.isConnected()) {
			return;
		}
		try {
			//set the running flag to true
			this.running=true;
			String host = this.properties.getProperty(TCPIPConnector.HOST);
			String defaultIPAddress = this.properties.getProperty(TCPIPConnector.DEFAULT_IPADDRESS);
			int port = Integer.parseInt(this.properties.getProperty(TCPIPConnector.PORT));
			Boolean.parseBoolean(this.properties.getProperty(TCPIPConnector.KEEP_ALIVE));
			this.keepAlive = true;
			
			
			InetSocketAddress inetAddress;
			
			//this is done to keep backward compatibility prior 2.0.4 release
			if(host == null || host.equals("")) {
				//check if defaultIp is also empty
				if (defaultIPAddress == null || defaultIPAddress.equals(""))
					throw new UnknownHostException();
				//otherwise start listening on defaultIp
				else
				{
					inetAddress = new InetSocketAddress(defaultIPAddress, port);
				}
			}
			//start listening on host
			else
			{
				inetAddress = new InetSocketAddress(host, port);
			}
				
			// open the socket channel
			this.channel = SocketChannel.open(inetAddress);
			this.channel.configureBlocking(false);
			this.channel.socket().setTcpNoDelay(true);
			
			//Check if the reader channel is not null, may happen if a disconnect has ocurred
			if (this.reader==null)
				this.reader = new SocketChannelReader(this);
			
			// assign the channel to the reader.
			this.reader.setChannel(this.channel);
			this.reader.start();
			
		} catch (UnknownHostException ex) {
			throw new ClientConnectorException("The host is unknown: " + ex.getMessage(), ex);
		} catch (SocketException ex) {
			throw new ClientConnectorException("There was an error in TCP/IP connection:" + ex.getMessage(), ex);
		} catch (ClosedChannelException ex) {
			throw new ClientConnectorException("The socket channel was closed: " + ex.getMessage(), ex);
		} catch (IOException ex) {
			throw new ClientConnectorException("There was an I/O exception in openning/using the socket channel: " + ex.getMessage(), ex);
		} catch(Exception e) {
			e.printStackTrace();
			throw new ClientConnectorException(e.getMessage(), e);
		}
	}
	
	/**
	 * Disconnect the client stop our readers and close the channel.
	 * 
	 * @return	None
	 * 
	 */
	public void disconnect() throws ClientConnectorException {

		if(channel == null) {
			return;
		}
		
		try {
			channel.close();
			channel = null;
			//Call destroy method so the SocketChannelReader and the TcpIpConnector threads are terminated gracefully
			
			destroy();
		} catch (IOException ex) {
			throw new ClientConnectorException("There was an I/O exception in openning/using the socket channel: " + ex.getMessage(), ex);
		}
	}    

	/** 
	 * Send a message to the server.
	 * 
	 * @param	cm		A ClientMessage object representing the message to be sent to the SIB
	 * @return	None
	 */
	private void writeMessage(ClientMessage cm) throws ClientConnectorException {
		try {
			// Calculate how many chunks are needed to send whole message.
			int count = cm.getPayloadSize() / writeBuffer.capacity();
			if(cm.getPayloadSize() % writeBuffer.capacity() > 0) {
				count++;
			}
			
			// Send the chunks 
			int offset = 0;
			int lenght = (cm.getPayloadSize() >= writeBuffer.capacity())? writeBuffer.capacity() : cm.getPayloadSize();
			for(int i = 0; i < count; i++) {
				writeBuffer.clear();
				writeBuffer.put(cm.getPayload(), offset, lenght);
				writeBuffer.flip();
				channelWrite(channel, writeBuffer);
				offset += writeBuffer.capacity();
				if(i == count-2) {
					lenght = cm.getPayloadSize() - offset;
				} else {
					lenght = writeBuffer.capacity();
				}
			}
			
		} catch (ClientConnectorException ex) {
			throw new ClientConnectorException("Cannot write the message in the socket channel" + ex.getMessage(), ex);
		}	
	}

	 /** 
     * Write the contents of a ByteBuffer to the given SocketChannel.
     * 
     * @param	channel		The SocketChannel object to be used when sending the contents from the buffer
     * @param	writeBuffer	The ByteBuffer object that contains the bytes to be sent to the user
     * @return	None
     * 
     */
    private void channelWrite(SocketChannel channel, ByteBuffer writeBuffer) throws ClientConnectorException {
		long nbytes = 0;
		long toWrite = writeBuffer.remaining();
	
		// loop on the channel.write() call since it will not necessarily
		// write all bytes in one shot
		try {
		    while (nbytes != toWrite) {
		    	nbytes += channel.write(writeBuffer);
		    }
		} catch (ClosedChannelException cce) {
			cce.printStackTrace();
			throw new ClientConnectorException("The socket was closed: " + cce.getMessage(), cce);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientConnectorException("Cannot write data in the socket channel: " + e.getMessage(), e);
		} 
	
		// get ready for another write if needed
		writeBuffer.rewind();

    }

    /** 
     * Adds a new message in the output queue
     * 
     * @param	msg		An array of bytes containing the data to be sent
     * @return	None
     * 
     */
	@Override
	public void write(byte[] msg) {
		outQueue.add(new ClientMessage(msg));
		synchronized(lock) {
			lock.notifyAll();
		}
	}	
	
	/**
	 * Adds a incoming message to the incoming messages queue. 
	 * 
	 * @param 	cm 	The ClientMessage object that contains the data received from the SIB
	 * @return	None
	 */
	public void addIncomingMessage(ClientMessage cm) {
		inQueue.add(cm);
		synchronized(lock) {
			lock.notifyAll();
		}
	}
	
	
	/**
	 * Used to gracefully stop both writing and reading threads from the TcpIpConnector object 
	 * 
	 * @return	None
	 */
	public void destroy() {
		
		if (this.keepAlive){
			//set control flag
			running = false;
			
			if(reader != null) {
				reader.shutdown();
			}
			
			reader = null;
			thread = null;
			//we must notify locking threads that disconnection actually happened
			synchronized(lock) {
				lock.notifyAll();
			}
		}
		//if keep alive is set to false, we should not stop the main thread, only the reading one
		else{
			if(reader != null) {
				reader.shutdown();
			}
			
			reader = null;
		}
		
		
	}
	
	/**
	 * Getters and setters
	 */
	public ConnectionSupport getConnectionSupport() {
		return connectionListeners;
	}
	@Override
	public String getName() {
		return "TCP/IP";
	}
	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);
		// Establish the keepAlive property.
		String tmp = this.properties.getProperty(TCPIPConnector.KEEP_ALIVE); 
		if(tmp != null) {
			this.keepAlive = Boolean.parseBoolean(tmp);
		}
	}
	
	
}
