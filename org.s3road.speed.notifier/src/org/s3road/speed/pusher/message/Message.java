/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *******************************************************************************/ 

package org.s3road.speed.pusher.message;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import com.google.common.primitives.Chars;


/**
 * This abstract class represents a Message to be sent
 * 
 */
public class Message {

	
	/** The transaction identifier */
	protected static long transactionId;
		
	/** The set of parameters */
	private String payload;
	
	/**
	 * Public constructor
	 */
	public Message() {
		transactionId++;
	}
	

	/**
	 * Sets the transaction identifier
	 * @param transactionId a long with the transaction identifier
	 */
	public void setTransactionId(long transactionId) {
		Message.transactionId = transactionId;
	}
	
	/**
	 * Gets the transaction identifier
	 * @return a long with the transaction identifier
	 */
	public long getTransactionId() {
		return transactionId;
	}
	
	
	public String getPayload() {
		return payload;
	}


	public void setPayload(String payload) {
		this.payload = payload;
	}


	/**
	 * Gets the string representation of this Message
	 * @return a string representation of the Message
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(openXML());
		sb.append(contentXML());
		sb.append(closeXML());
		return sb.toString();
	}
	
	/**
	 * Returns the opening tag of a Message
	 * @return a string with the opening tag of a XML representation of Message
	 */
	protected String openXML() {
		return "<Message>\n";
	}
	
	/**
	 * Gets the content of the SSAPMessage, including parameters
	 * @return a string representation of the content of a SSAPMessage
	 */
	protected String contentXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("  <transaction_id>");
		sb.append(transactionId);
		sb.append("</transaction_id>\n");
		sb.append("  <payload>");
		sb.append(payload);
		sb.append("</payload>\n");
		return sb.toString();
	}
	
	/**
	 * Returns the opening tag of a SSAPMessage
	 * @return a string with the closing tag of a XML representation of SSAPMessage
	 */
	protected String closeXML() {
		return "</Message>";
	}
	
	/**
	 * Gets the byte[] representation of the SSAPMessage
	 * @return a byte[] array
	 */
	public byte[] toByteArray() {
		if (Charset.isSupported("UTF-8")) {
			return toString().getBytes(Charset.forName("UTF-8"));
		}
		else {
			return toString().getBytes();
		}
		
	}
	
	/**
	 * Returns the Characters representation of the payload String
	 * @param payload	The payload as String
	 * 	@return	The ArrayList containing the payload characters
	 */
	public List<Character> getCharactersFromPayload(String payload){    
		List<Character> ret = new ArrayList<Character>();
		ret = Chars.asList("abc".toCharArray());
	    return ret;
	}
}
