/*******************************************************************************
 * Copyright (c) 2013 Tecnalia Research and Innovation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *******************************************************************************/

package org.s3road.speed.pusher;

import java.io.IOException;
import java.util.Properties;
import java.util.Random;

import org.s3road.speed.pusher.connector.ClientConnectorException;
import org.s3road.speed.pusher.connector.impl.TCPIPConnector;
import org.s3road.speed.pusher.message.Message;
import org.smool.kpi.common.Logger;

/**
 * Class that pushes random speed messages to the server
 * @author Alejandro Villamarin - Tecnalia alejandro.villamarin@tecnalia.com
 *
 */
public class Pusher implements Runnable {

	
	/**
	 * The thread object
	 */
	private Thread thread;
	
	/**
	 * Lock to avoid expensive run()
	 */
	private Object lock;
	
	/**
	 * Control flag
	 */
	private boolean running;
	
	/**
	 * Speed to be sent in messages
	 */
	private int speed;
	
	/**
	 * Used to create random speeds
	 */
	private Random rand;
	
	/**
	 * Lower speed limit
	 */
	int min;
	
	/**
	 * Upper speed limit
	 */
	int max;
	
	/**
	 * The connector reference
	 */
	private TCPIPConnector connector;
	
	/**
	 * Constants to be used for the connector properties
	 */
	private String server_address = "192.168.1.223";
    private String server_port = "23000";


    /**
     * Determines the sleeping time between sending messages
     */
	private long send_interval = 30000;
	
    /**
     * Properties for the connector
     */
    private Properties connectorProperties;
    
	/**
	 * Constructor
	 * @param	port		The server port
	 * @param	address		The server address
	 * @param	interval	The message sending interval period, specified in seconds
	 * @throws IOException
	 */
    public Pusher(int port, String address, int interval) throws IOException {
    	
    	rand = new Random();
    	min = 50;
    	max = 120;
		this.lock = new Object();
		server_port = String.valueOf(port);
		server_address = address;
		
		//create the connector and start the threads
		try {
			connector = new TCPIPConnector();
			connectorProperties = new Properties();
			connectorProperties.setProperty("DEFAULT_IPADDRESS", server_address);
			connectorProperties.setProperty("PORT", server_port); 
			connector.setProperties(connectorProperties);
		}
		catch (IOException e) {
			Logger.error(this.getClass().getName(), "Could not create the connector reference. Aborting. Reason is " + e.getMessage());
			throw new IOException(e);
		}
		
    }
    
    /**
     * Starts the thread
     */
    public void start(){
    	//Try to connect
    	try {
    		connector.connect();
    		Logger.info(this.getClass().getName(), "Connected to Server, proceeding to send messages");
    		// start processing outoging messages
    		this.thread = new Thread(this, "Pusher");
    		this.thread.start();
    	} 
    	catch (ClientConnectorException e) {
    		Logger.error(this.getClass().getName(), "Could not connect to server. Cannot send messages. Aborting");
    		System.exit(1);
    	}
    }
	
	@Override
	public void run() {
		running = true;
		Logger.debug(this.getClass().getName(), "Pusher Thread starting");
		
		while(running) {
			//create a random speed integer
			speed = rand.nextInt(max - min + 1) + min;
			//Create a new Message with the new speed and send it
			Message message = new Message();
			StringBuilder buffer = new StringBuilder();
			buffer.append("Speed: ");
			buffer.append(speed);
			message.setPayload(buffer.toString());
			Logger.debug(this.getClass().getName(), "Sending message: " + message.toString());
			connector.write(message.toByteArray());
			//Sleep for some seconds, since we don't expect any answer from the server
			try {
                Thread.sleep(send_interval);
            } 
            catch (InterruptedException e) { 
            	Logger.debug(this.getClass().getName(), "Pusher Thread has been interrupted while sleeping for the next message. Reason is " + e.getMessage());
            }
		}
		Logger.debug(this.getClass().getName(), "Pusher Thread stopped");
	}
	
	/**
	 * Used to gracefully stop the thread
	 * 
	 * @return	None
	 */
	public void destroy() {
			
		//tell the connector to stop its thread too
		connector.destroy();
		//set control flag
		running = false;
		thread = null;
		//we must notify locking threads that disconnection actually happened
		synchronized(lock) {
			lock.notifyAll();
		}
	}



}
